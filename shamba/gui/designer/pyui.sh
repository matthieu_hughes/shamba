#!/bin/bash

function pyui()
{
    pyuic4 -x $1 -o $2
    chmod +x $2
    sed -i '1i #!/usr/bin/python' $2
}

for u in *.ui
do
    p=$(sed 's/.ui/_ui.py/g' <<< $u)
    pyui $u $p
done

