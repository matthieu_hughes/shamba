# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'shamba/gui/designer/main.ui'
#
# Created: Mon Aug  4 16:58:01 2014
#      by: PyQt4 UI code generator 4.6.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_SHAMBA(object):
    def setupUi(self, SHAMBA):
        SHAMBA.setObjectName("SHAMBA")
        SHAMBA.resize(999, 756)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(SHAMBA.sizePolicy().hasHeightForWidth())
        SHAMBA.setSizePolicy(sizePolicy)
        SHAMBA.setMinimumSize(QtCore.QSize(0, 0))
        self.centralwidget = QtGui.QWidget(SHAMBA)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_7 = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setMaximumSize(QtCore.QSize(9999999, 9999999))
        self.tabWidget.setObjectName("tabWidget")
        self.generalTab = QtGui.QWidget()
        self.generalTab.setObjectName("generalTab")
        self.gridLayout_3 = QtGui.QGridLayout(self.generalTab)
        self.gridLayout_3.setObjectName("gridLayout_3")
        spacerItem = QtGui.QSpacerItem(647, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout_3.addItem(spacerItem, 0, 1, 1, 1)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem1 = QtGui.QSpacerItem(231, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem1)
        self.generalButton = QtGui.QPushButton(self.generalTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.generalButton.sizePolicy().hasHeightForWidth())
        self.generalButton.setSizePolicy(sizePolicy)
        self.generalButton.setMinimumSize(QtCore.QSize(271, 50))
        self.generalButton.setMaximumSize(QtCore.QSize(271, 16777215))
        self.generalButton.setObjectName("generalButton")
        self.horizontalLayout_4.addWidget(self.generalButton)
        spacerItem2 = QtGui.QSpacerItem(231, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem2)
        self.gridLayout_3.addLayout(self.horizontalLayout_4, 1, 1, 1, 1)
        self.generalOutputBox = QtGui.QTextEdit(self.generalTab)
        self.generalOutputBox.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.generalOutputBox.sizePolicy().hasHeightForWidth())
        self.generalOutputBox.setSizePolicy(sizePolicy)
        self.generalOutputBox.setMinimumSize(QtCore.QSize(650, 500))
        self.generalOutputBox.setMaximumSize(QtCore.QSize(9999999, 9999999))
        self.generalOutputBox.setReadOnly(True)
        self.generalOutputBox.setObjectName("generalOutputBox")
        self.gridLayout_3.addWidget(self.generalOutputBox, 2, 1, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(100, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem3, 2, 0, 1, 1)
        spacerItem4 = QtGui.QSpacerItem(100, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem4, 2, 2, 1, 1)
        spacerItem5 = QtGui.QSpacerItem(20, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout_3.addItem(spacerItem5, 3, 1, 1, 1)
        self.tabWidget.addTab(self.generalTab, "")
        self.baselinesTab = QtGui.QWidget()
        self.baselinesTab.setObjectName("baselinesTab")
        self.gridLayout_9 = QtGui.QGridLayout(self.baselinesTab)
        self.gridLayout_9.setObjectName("gridLayout_9")
        spacerItem6 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout_9.addItem(spacerItem6, 0, 1, 1, 1)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.baselineSelector = QtGui.QComboBox(self.baselinesTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.baselineSelector.sizePolicy().hasHeightForWidth())
        self.baselineSelector.setSizePolicy(sizePolicy)
        self.baselineSelector.setMinimumSize(QtCore.QSize(271, 30))
        self.baselineSelector.setMaximumSize(QtCore.QSize(271, 30))
        self.baselineSelector.setMaxCount(100)
        self.baselineSelector.setObjectName("baselineSelector")
        self.baselineSelector.addItem("")
        self.verticalLayout_3.addWidget(self.baselineSelector)
        self.horizontalLayout_3.addLayout(self.verticalLayout_3)
        self.label_3 = QtGui.QLabel(self.baselinesTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.baselineButton = QtGui.QPushButton(self.baselinesTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.baselineButton.sizePolicy().hasHeightForWidth())
        self.baselineButton.setSizePolicy(sizePolicy)
        self.baselineButton.setMinimumSize(QtCore.QSize(271, 50))
        self.baselineButton.setMaximumSize(QtCore.QSize(271, 50))
        self.baselineButton.setObjectName("baselineButton")
        self.horizontalLayout_3.addWidget(self.baselineButton)
        self.gridLayout_9.addLayout(self.horizontalLayout_3, 1, 1, 1, 1)
        spacerItem7 = QtGui.QSpacerItem(100, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem7, 2, 0, 1, 1)
        spacerItem8 = QtGui.QSpacerItem(100, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout_9.addItem(spacerItem8, 2, 2, 1, 1)
        spacerItem9 = QtGui.QSpacerItem(20, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout_9.addItem(spacerItem9, 3, 1, 1, 1)
        self.baselineOutputBox = QtGui.QTextEdit(self.baselinesTab)
        self.baselineOutputBox.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.baselineOutputBox.sizePolicy().hasHeightForWidth())
        self.baselineOutputBox.setSizePolicy(sizePolicy)
        self.baselineOutputBox.setMinimumSize(QtCore.QSize(650, 500))
        self.baselineOutputBox.setMaximumSize(QtCore.QSize(9999999, 9999999))
        self.baselineOutputBox.setReadOnly(True)
        self.baselineOutputBox.setObjectName("baselineOutputBox")
        self.gridLayout_9.addWidget(self.baselineOutputBox, 2, 1, 1, 1)
        self.tabWidget.addTab(self.baselinesTab, "")
        self.interventionsTab = QtGui.QWidget()
        self.interventionsTab.setObjectName("interventionsTab")
        self.gridLayout = QtGui.QGridLayout(self.interventionsTab)
        self.gridLayout.setObjectName("gridLayout")
        spacerItem10 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem10, 0, 1, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.interventionSelector = QtGui.QComboBox(self.interventionsTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.interventionSelector.sizePolicy().hasHeightForWidth())
        self.interventionSelector.setSizePolicy(sizePolicy)
        self.interventionSelector.setMinimumSize(QtCore.QSize(271, 30))
        self.interventionSelector.setMaximumSize(QtCore.QSize(271, 30))
        self.interventionSelector.setMaxCount(100)
        self.interventionSelector.setObjectName("interventionSelector")
        self.interventionSelector.addItem("")
        self.verticalLayout.addWidget(self.interventionSelector)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.label_5 = QtGui.QLabel(self.interventionsTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout.addWidget(self.label_5)
        self.interventionButton = QtGui.QPushButton(self.interventionsTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.interventionButton.sizePolicy().hasHeightForWidth())
        self.interventionButton.setSizePolicy(sizePolicy)
        self.interventionButton.setMinimumSize(QtCore.QSize(271, 50))
        self.interventionButton.setMaximumSize(QtCore.QSize(271, 50))
        self.interventionButton.setObjectName("interventionButton")
        self.horizontalLayout.addWidget(self.interventionButton)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 1, 1, 1)
        self.interventionOutputBox = QtGui.QTextEdit(self.interventionsTab)
        self.interventionOutputBox.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.interventionOutputBox.sizePolicy().hasHeightForWidth())
        self.interventionOutputBox.setSizePolicy(sizePolicy)
        self.interventionOutputBox.setMinimumSize(QtCore.QSize(650, 500))
        self.interventionOutputBox.setMaximumSize(QtCore.QSize(9999999, 9999999))
        self.interventionOutputBox.setReadOnly(True)
        self.interventionOutputBox.setObjectName("interventionOutputBox")
        self.gridLayout.addWidget(self.interventionOutputBox, 2, 1, 2, 1)
        spacerItem11 = QtGui.QSpacerItem(100, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem11, 2, 2, 1, 1)
        spacerItem12 = QtGui.QSpacerItem(100, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem12, 3, 0, 1, 1)
        spacerItem13 = QtGui.QSpacerItem(20, 10, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem13, 4, 1, 1, 1)
        self.tabWidget.addTab(self.interventionsTab, "")
        self.mitigationTab = QtGui.QWidget()
        self.mitigationTab.setObjectName("mitigationTab")
        self.gridLayout_2 = QtGui.QGridLayout(self.mitigationTab)
        self.gridLayout_2.setObjectName("gridLayout_2")
        spacerItem14 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout_2.addItem(spacerItem14, 0, 0, 1, 1)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(20)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem15 = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem15)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setSpacing(8)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.baselineSelectorPlot = QtGui.QComboBox(self.mitigationTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.baselineSelectorPlot.sizePolicy().hasHeightForWidth())
        self.baselineSelectorPlot.setSizePolicy(sizePolicy)
        self.baselineSelectorPlot.setMinimumSize(QtCore.QSize(271, 30))
        self.baselineSelectorPlot.setMaximumSize(QtCore.QSize(271, 30))
        self.baselineSelectorPlot.setObjectName("baselineSelectorPlot")
        self.baselineSelectorPlot.addItem("")
        self.verticalLayout_4.addWidget(self.baselineSelectorPlot)
        self.interventionSelectorPlot = QtGui.QComboBox(self.mitigationTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.interventionSelectorPlot.sizePolicy().hasHeightForWidth())
        self.interventionSelectorPlot.setSizePolicy(sizePolicy)
        self.interventionSelectorPlot.setMinimumSize(QtCore.QSize(271, 30))
        self.interventionSelectorPlot.setMaximumSize(QtCore.QSize(271, 30))
        self.interventionSelectorPlot.setObjectName("interventionSelectorPlot")
        self.interventionSelectorPlot.addItem("")
        self.verticalLayout_4.addWidget(self.interventionSelectorPlot)
        self.horizontalLayout_2.addLayout(self.verticalLayout_4)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setSpacing(8)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        spacerItem16 = QtGui.QSpacerItem(10, 20, QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem16)
        self.mitigationButton = QtGui.QPushButton(self.mitigationTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mitigationButton.sizePolicy().hasHeightForWidth())
        self.mitigationButton.setSizePolicy(sizePolicy)
        self.mitigationButton.setMinimumSize(QtCore.QSize(231, 50))
        self.mitigationButton.setMaximumSize(QtCore.QSize(231, 50))
        self.mitigationButton.setObjectName("mitigationButton")
        self.horizontalLayout_2.addWidget(self.mitigationButton)
        self.graphSaveButton = QtGui.QPushButton(self.mitigationTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.graphSaveButton.sizePolicy().hasHeightForWidth())
        self.graphSaveButton.setSizePolicy(sizePolicy)
        self.graphSaveButton.setMinimumSize(QtCore.QSize(231, 50))
        self.graphSaveButton.setObjectName("graphSaveButton")
        self.horizontalLayout_2.addWidget(self.graphSaveButton)
        spacerItem17 = QtGui.QSpacerItem(10, 20, QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem17)
        self.gridLayout_2.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.plotWidget = PlotWidget(self.mitigationTab)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.plotWidget.sizePolicy().hasHeightForWidth())
        self.plotWidget.setSizePolicy(sizePolicy)
        self.plotWidget.setObjectName("plotWidget")
        self.gridLayout_2.addWidget(self.plotWidget, 2, 0, 1, 1)
        self.tabWidget.addTab(self.mitigationTab, "")
        self.gridLayout_7.addWidget(self.tabWidget, 0, 0, 1, 1)
        self.helpButton = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.helpButton.sizePolicy().hasHeightForWidth())
        self.helpButton.setSizePolicy(sizePolicy)
        self.helpButton.setObjectName("helpButton")
        self.gridLayout_7.addWidget(self.helpButton, 1, 0, 1, 1)
        SHAMBA.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(SHAMBA)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 999, 25))
        self.menubar.setObjectName("menubar")
        self.fileMenu = QtGui.QMenu(self.menubar)
        self.fileMenu.setTearOffEnabled(True)
        self.fileMenu.setObjectName("fileMenu")
        SHAMBA.setMenuBar(self.menubar)
        self.actionOpen = QtGui.QAction(SHAMBA)
        self.actionOpen.setObjectName("actionOpen")
        self.actionNew = QtGui.QAction(SHAMBA)
        self.actionNew.setObjectName("actionNew")
        self.actionSave = QtGui.QAction(SHAMBA)
        self.actionSave.setObjectName("actionSave")
        self.actionSave_As = QtGui.QAction(SHAMBA)
        self.actionSave_As.setObjectName("actionSave_As")
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.actionSave)
        self.fileMenu.addAction(self.actionSave_As)
        self.fileMenu.addSeparator()
        self.menubar.addAction(self.fileMenu.menuAction())

        self.retranslateUi(SHAMBA)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(SHAMBA)

    def retranslateUi(self, SHAMBA):
        SHAMBA.setWindowTitle(QtGui.QApplication.translate("SHAMBA", "SHAMBA", None, QtGui.QApplication.UnicodeUTF8))
        self.generalButton.setText(QtGui.QApplication.translate("SHAMBA", "Enter general project information", None, QtGui.QApplication.UnicodeUTF8))
        self.generalOutputBox.setHtml(QtGui.QApplication.translate("SHAMBA", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.generalTab), QtGui.QApplication.translate("SHAMBA", "General", None, QtGui.QApplication.UnicodeUTF8))
        self.baselineSelector.setItemText(0, QtGui.QApplication.translate("SHAMBA", "<Select a saved baseline scenario>", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("SHAMBA", "or", None, QtGui.QApplication.UnicodeUTF8))
        self.baselineButton.setText(QtGui.QApplication.translate("SHAMBA", "Add a new baseline scenario", None, QtGui.QApplication.UnicodeUTF8))
        self.baselineOutputBox.setHtml(QtGui.QApplication.translate("SHAMBA", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.baselinesTab), QtGui.QApplication.translate("SHAMBA", "Baseline Scenario", None, QtGui.QApplication.UnicodeUTF8))
        self.interventionSelector.setItemText(0, QtGui.QApplication.translate("SHAMBA", "<Select a saved intervention>", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("SHAMBA", "or", None, QtGui.QApplication.UnicodeUTF8))
        self.interventionButton.setText(QtGui.QApplication.translate("SHAMBA", "Add a new intervention", None, QtGui.QApplication.UnicodeUTF8))
        self.interventionOutputBox.setHtml(QtGui.QApplication.translate("SHAMBA", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans\'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.interventionsTab), QtGui.QApplication.translate("SHAMBA", "Intervention", None, QtGui.QApplication.UnicodeUTF8))
        self.baselineSelectorPlot.setItemText(0, QtGui.QApplication.translate("SHAMBA", "<Select a saved baseline scenario>", None, QtGui.QApplication.UnicodeUTF8))
        self.interventionSelectorPlot.setItemText(0, QtGui.QApplication.translate("SHAMBA", "<Select a saved intervention>", None, QtGui.QApplication.UnicodeUTF8))
        self.mitigationButton.setText(QtGui.QApplication.translate("SHAMBA", "Plot mitigation estimates", None, QtGui.QApplication.UnicodeUTF8))
        self.graphSaveButton.setText(QtGui.QApplication.translate("SHAMBA", "Save graph", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.mitigationTab), QtGui.QApplication.translate("SHAMBA", "Mitigation Estimates", None, QtGui.QApplication.UnicodeUTF8))
        self.helpButton.setText(QtGui.QApplication.translate("SHAMBA", "Help", None, QtGui.QApplication.UnicodeUTF8))
        self.fileMenu.setTitle(QtGui.QApplication.translate("SHAMBA", "File", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOpen.setText(QtGui.QApplication.translate("SHAMBA", "Open", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOpen.setShortcut(QtGui.QApplication.translate("SHAMBA", "Ctrl+O", None, QtGui.QApplication.UnicodeUTF8))
        self.actionNew.setText(QtGui.QApplication.translate("SHAMBA", "New", None, QtGui.QApplication.UnicodeUTF8))
        self.actionNew.setShortcut(QtGui.QApplication.translate("SHAMBA", "Ctrl+N", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSave.setText(QtGui.QApplication.translate("SHAMBA", "Save", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSave.setShortcut(QtGui.QApplication.translate("SHAMBA", "Ctrl+S", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSave_As.setText(QtGui.QApplication.translate("SHAMBA", "Save As...", None, QtGui.QApplication.UnicodeUTF8))

from shamba.gui.plot import PlotWidget

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    SHAMBA = QtGui.QMainWindow()
    ui = Ui_SHAMBA()
    ui.setupUi(SHAMBA)
    SHAMBA.show()
    sys.exit(app.exec_())

