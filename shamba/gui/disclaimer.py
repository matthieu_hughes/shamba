from PyQt4 import QtCore, QtGui

from shamba.gui.translate_ import translate_ as _
from shamba.gui import designer
from shamba.gui.designer.disclaimer_ui import Ui_Disclaimer
import sys
import os


class DisclaimerDialog(QtGui.QDialog):

    def __init__(self, disclaimer_file, splash_file, parent=None):
        """Show the dislaimer dialog
        Args:
            disclaimer_file: path to text file to show in the dialog
            splash_file: path to image (jpg format) to show in the dialog

        """
        with open(disclaimer_file) as f:
            disclaimer = f.read()
        
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_Disclaimer()
        self.ui.setupUi(self)
    
        pix = QtGui.QPixmap(splash_file)
        self.ui.splashImage.setPixmap(pix.scaled(self.ui.splashImage.size(),QtCore.Qt.KeepAspectRatio))
        self.ui.disclaimerText.setText(_(disclaimer))

    def reject(self):
        sys.exit(0)

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    myapp = DisclaimerDialog(sys.argv[1], sys.argv[2])
    myapp.show()
    sys.exit(app.exec_())
