#!/usr/bin/python

"""Main driver for SHAMBA command-line mode."""
import logging as log
import os
import sys

import matplotlib.pyplot as plt
import numpy as np

_dir = os.path.dirname(os.path.abspath(__file__))
print _dir
sys.path.append(os.path.dirname(_dir))
print sys.path

from shamba.model.climate import Climate
from shamba.model.soil_params import SoilParams

from shamba.model.crop_params import CropParams
from shamba.model.crop_model import CropModel

from shamba.model.tree_params import TreeParams
from shamba.model.tree_growth import TreeGrowth
from shamba.model.tree_model import TreeModel

from shamba.model.litter import LitterModel
from shamba.model.soil_model import InverseRothC, ForwardRothC
from shamba.model import cfg, emit, io_


def main():
    """Main function where everything happens."""
    # Initial stuff
    cfg.args = io_.get_cl_args()
    #clay = float(cfg.args.param)

    cfg.SAV_DIR = os.path.join(cfg.PROJ_DIR, 'sensitivity')
    cfg.INP_DIR = os.path.join(cfg.SAV_DIR, 'input')
    cfg.OUT_DIR = os.path.join(cfg.SAV_DIR, 'output')

    loc = (-18.987214, 34.174625)
    climate = Climate.from_location(loc)
        
    # Set model run length from max tree age data available
    #cfg.N_YEARS = int(treeGrowth.age.max() + 20)
    cfg.N_YEARS = 50
    cfg.N_ACCT = cfg.N_YEARS
    emit.FIRE = np.zeros(cfg.N_YEARS)

    # Equilibrium solve
    soil = SoilParams.from_location(loc)
    invRoth = InverseRothC(soil,climate)
        
        
    # ----------
    # tree model
    # ----------
        
    # do growth stuff
    tree_par = TreeParams.from_species_name('default')
    growth = TreeGrowth.from_csv(tree_par, filename='growth_test.csv')
    
    # run tree model
    thinning = np.zeros(cfg.N_YEARS+1)
    thinning[9] = 0.5
    mort = np.array((cfg.N_YEARS+1) * [0.01])
    tree1 = TreeModel.from_defaults(
            tree_params=tree_par, tree_growth=growth, 
            thin=thinning, mort=mort) 
   
    # ----------
    # Crop model
    # ----------
    
    # Baseline
    cropPar = io_.read_csv('crop_ipcc_baseline.csv')
    cropPar = np.atleast_2d(cropPar)
    crop_base = []   # list for crop objects
    crop_par_base = []
    for row in cropPar: # loop through rows
        spp = int(row[0])
        harvYield = row[1]
        harvFrac = row[2]
       
        ci = CropParams.from_species_index(spp)
        c = CropModel(ci, harvYield, harvFrac)
        crop_base.append(c)
        crop_par_base.append(ci)

    # Project 
    cropPar = io_.read_csv('crop_ipcc.csv')
    cropPar = np.atleast_2d(cropPar)
    crop_proj = []
    crop_par_proj = []
    for row in cropPar:
        spp = int(row[0])
        harvYield = row[1]
        harvFrac = row[2]

        ci = CropParams.from_species_index(spp)
        c = CropModel(ci, harvYield, harvFrac)
        crop_proj.append(c)
        crop_par_proj.append(ci)

    # soil cover for project
    coverf = np.ones(12)
    coverf[7:11] = 0     # bare August to November
    # Solve to y=0
    forRoth = ForwardRothC(
            soil, climate, coverf, Ci=invRoth.eqC, 
            crop=crop_base, solveToValue=True)
    
    # Soil carbon for baseline and project
    roth_base = ForwardRothC(
            soil, climate, coverf, Ci=forRoth.SOC[-1], crop=crop_base)
    
    roth_proj = ForwardRothC(
            soil, climate, coverf, Ci=forRoth.SOC[-1],
            crop=crop_proj, tree=[tree1])

    # Emissions stuff
    emit_base = emit.Emission(
            forRothC=roth_base,
            crop=crop_base)
    emit_proj = emit.Emission(
            forRothC=roth_proj,
            crop=crop_proj,
            tree=[tree1])
    
    
    # Print stuff:
    # ** for sensitivity stuff, only print the total project emission
    # and the independent variable (e.g. stand density)
    #print "%f,%f" % (evap, sum(emit_proj.emissions))

    
    print "location: ",loc
    climate.print_()
    soil.print_()
    growth.print_()
    tree1.print_biomass()
    tree1.print_balance()
    forRoth.print_()
    roth_base.print_()
    roth_proj.print_()
    
    print "\n\nEMISSIONS (t CO2)"
    print "=================\n"
    print "baseline    project"
    emit_diff = emit_proj.emissions - emit_base.emissions
    for i in range(len(emit_base.emissions)):
        print emit_base.emissions[i],emit_proj.emissions[i],emit_diff[i]

    print "\nTotal difference: ",sum(emit_diff), " t CO2 ha^-1"
    print "Average difference: ", np.mean(emit_diff)
      
    # Save stuff 
    climate.save_()
    soil.save_()
    growth.save_()
    tree_par.save_()
    tree1.save_()
    i = 1
    for i in range(len(crop_base)):
        crop_base[i].save_("crop_model_base_"+str(i)+".csv")
        crop_par_base[i].save_("crop_params_base_"+str(i)+".csv")
        crop_proj[i].save_("crop_model_proj_"+str(i)+".csv")
        crop_par_proj[i].save_("crop_params_proj_"+str(i)+".csv")

    invRoth.save_()
    forRoth.save_()
    roth_base.save_('soil_model_base.csv')
    roth_proj.save_('soil_model_proj.csv')
    emit_proj.save_(emit_base, emit_proj)

    # Plot stuff
    growth.plot_(saveName='growthFits.png')
    tree1.plot_biomass(saveName='biomassPools.png')
    tree1.plot_balance(saveName='massBalance.png')
    forRoth.plot_(legendStr='initialisation')
    roth_base.plot_(legendStr='baseline')
    roth_proj.plot_(legendStr='project', saveName='soilModel.png')
    emit_base.plot_(legendStr='baseline')
    emit_proj.plot_(legendStr='project')
    emit.Emission.ax.plot(emit_diff, label='difference')
    emit.Emission.ax.legend(loc='best')
    plt.savefig(os.path.join(cfg.OUT_DIR, 'emissions.png'))
    
    emit_proj.save_(emit_base, emit_proj=emit_proj)
    
    plt.show()


if __name__ == '__main__':
    main()
