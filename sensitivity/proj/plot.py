#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt


def load_(filename):
    return np.loadtxt(filename, delimiter=',')

#sd100 = (load_('emissions_standdens100.csv'), "100 trees / ha")
#sd200 = (load_('emissions_standdens200.csv'), "200 trees / ha")
#sd300 = (load_('emissions_standdens300.csv'), "300 trees / ha")
#sd400 = (load_('emissions_standdens400.csv'), "400 trees / ha")
#sd500 = (load_('emissions_standdens500.csv'), "500 trees / ha")

data = load_('emissions_vs_stand_dens.csv')

if __name__ == '__main__':
    fig = plt.figure()
    fig.canvas.set_window_title("Emissions vs stand density")
    ax = fig.add_subplot(1,1,1)
    ax.set_title("Emissions vs Stand density")
    ax.set_xlabel("Stand density (trees / ha)")
    ax.set_ylabel("Net emissions (t CO_2 / ha)")
    
    ax.scatter(data[:,0], data[:,1])
    #for sd in [sd100, sd200, sd300, sd400, sd500]:
    #    ax.plot(sd[0], label=sd[1])

    #ax.legend(loc='best')
    
    plt.savefig('emissions_vs_stand_dens.png')
    plt.show()

