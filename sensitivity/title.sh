#!/bin/bash

head=",emissions,x,S"

mkdir "$1"/with_titles

for f in "$1"/*; do
    par=$(basename $f .csv)
    header=$par$head
    echo "$header" > "$1"/.temp
    cat "$f" >> "$1"/.temp
    
    new_fname=$par
    new_fname+=".csv"
    mv "$1"/.temp "$1"/with_titles/"$new_fname"
done
