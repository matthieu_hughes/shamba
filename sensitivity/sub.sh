#!/bin/bash

#for p in 'thin' 'mort' 'carbon' 'stand_dens' 'root_to_shoot' 'alloc_branch' 'alloc_croot' 'alloc_froot' 'alloc_leaf' 'alloc_stem' 'nitrogen_branch' 'nitrogen_croot' 'nitrogen_froot' 'nitrogen_leaf' 'nitrogen_stem' 'turnover_branch' 'turnover_froot' 'turnover_leaf'; do


for p in 'bgn' 'agn' 'bgc' 'agc' 'root_to_shoot'; do 
    echo "$p"
    ./sub.py crop "$p" > temp
    p+=".csv"
    mv temp crop2/"$p"
done

# run sens.py on the files
for f in crop2/*; do
    n=$(basename $f .csv)
    echo "$n"
    ./sens.py crop "$n" $f
done

# same for crops
#for p in 'yield' 'left_in_field' 'yield_baseline'; do
#    echo "$p"
#    ./sub.py crop "$p" > temp
#    p+=".csv"
#    mv temp crop/"$p"
#done
#for f in crop/*; do
#    n=$(basename $f .csv)
#    echo "$n"
#    ./sens.py crop "$n" $f
#done


