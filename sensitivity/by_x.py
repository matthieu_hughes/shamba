#!/usr/bin/python

from __future__ import print_function

import os
import sys
import numpy as np
from pprint import pprint

x_values_wanted = [0.5, 0.75, 1.5, 2]
S_values_wanted = {}

# walks the directory given as argv[1]
for dirpath, _, filename in os.walk(sys.argv[1]):
    for f in filename: 
        param_name = f.split(".csv")[0]
        try:
            param_data, emi_data, x_data, S_data = np.loadtxt(
                    os.path.join(dirpath, f), delimiter=',',  
                    skiprows=1, unpack=True
            )
        except ValueError: # skip files not structure the right way
            continue

        S_values_wanted[param_name] = []
        for x in x_values_wanted:
            try:
                index = np.where(abs(x_data-x) < 0.00001)[0][0]
                S_values_wanted[param_name].append((x, S_data[index]))
            except IndexError:  # doesn't have that particular x value
                pass
         
for par in S_values_wanted:
    print(par, end=',')
    for x,S in S_values_wanted[par]:
        print(S, end=',')
    print('')
