#!/usr/bin/python

import numpy as np
import sys

sys.path.append('/home/mhughes6/shamba')

from shamba.model.soil_params import SoilParams
from shamba.model.climate import Climate
import main_cl

# using num=41 for each ensures that S values of exactly 
# 0.25, 0.5, 1, 1.5 and 2 will be calculated 
def linspace41(n,m):
    return np.linspace(n,m,num=41)


tree_default = {
        'stand_dens': 200,
        'thin': 0.5,
        'mort': 0.01,
        'thin_frac': np.array([1,0,0,1,1]),
        'mort_frac': np.array([1,0,0,1,1]),
        'nitrogen_leaf': 0.02,
        'nitrogen_branch': 0.0015,
        'nitrogen_stem': 0.0015,
        'nitrogen_croot': 0.0015,
        'nitrogen_froot': 0.0113,
        'carbon': 0.5,
        'alloc_leaf': 0.1,
        'alloc_branch': 0.31,
        'alloc_stem': 0.69,
        'alloc_croot': 0.1794,
        'alloc_froot': 0.1,
        'turnover_leaf': 1,
        'turnover_branch': 0.05,
        'turnover_stem': 0,
        'turnover_croot': 0,
        'turnover_froot': 0.8,
        'root_to_shoot': 0.26,
        'density': 0.6,
        'allometric': 'ryan',
        'fert': 10,
        'litter': 10,
        'none': None,
        'growth': 1,
}

tree_vary = {
        'stand_dens': linspace41(0, 500),
        'thin': linspace41(0, 1),
        'mort': linspace41(0, 0.05),
        'nitrogen_leaf': linspace41(0, 0.04),
        'nitrogen_branch': linspace41(0, 0.003),
        'nitrogen_stem': linspace41(0, 0.003),
        'nitrogen_croot': linspace41(0, 0.003),
        'nitrogen_froot': linspace41(0, 0.0226),
        'carbon': linspace41(0, 1),
        'alloc_leaf': linspace41(0, 0.2),
        'alloc_branch': linspace41(0, 0.62),
        'alloc_stem': linspace41(0, 1.38),
        'alloc_croot': linspace41(0, 0.3588),
        'alloc_froot': linspace41(0, 0.2),
        'turnover_leaf': linspace41(0, 2),
        'turnover_branch': linspace41(0, 0.1),
        'turnover_stem': None,
        'turnover_croot': None,
        'turnover_froot': linspace41(0, 1.6),
        'root_to_shoot': linspace41(0, 0.52),
        'density': linspace41(0, 1.2),
        'allometric': [
                'ryan','grevillea','maesopsis',
                'chave dry', 'chave moist', 'chave wet'],
        'fert': np.linspace(1,20,num=20),
        'none': [None],
        'growth': linspace41(0,5)
}


crop_default = {
        'yield': 1.5,
        'yield_baseline': 1.0,
        'left_in_field': 0.5,
        'slope': 1.03,
        'intercept': 0.61,
        'agc': 0.4,
        'bgc': 0.4,
        'agn': 0.006,
        'bgn': 0.007,
        'root_to_shoot': 0.22
}

crop_vary = {
        'yield_baseline': linspace41(0, 5.0), 
        'yield': linspace41(0, 5.0),
        'left_in_field': linspace41(0,1),
        'slope': linspace41(0, 2.06),
        'intercept': linspace41(0, 1.22),
        'agc': linspace41(0, 0.8),
        'bgc': linspace41(0, 0.8),
        'agn': linspace41(0, 0.012),
        'bgn': linspace41(0, 0.014),
        'root_to_shoot': linspace41(0, 0.44)
}


location_default = (-18.987214, 34.174625)
soil_default = SoilParams.from_location(location_default)
climate_default = Climate.from_location(location_default)

soil_default = {
        'cover': np.array([1,1,1,1,1,1,1,0,0,0,0,1]),
        'Cy0': soil_default.Cy0,
        'Ceq_frac': 0.25,
        'clay': soil_default.clay,
        'temp': climate_default.temp,
        'rain': climate_default.rain,
        'evap': climate_default.evap,
        'location': location_default,
        'fire_freq': 10,
        'litter_qty': 1.0,
        'litter_freq': 1,
        'fert_qty': 1.0,
        'fert_freq': 1,
        'fert_nitrogen': 0.018,
}

soil_vary = {
        'cover': [
                    np.array([1,1,1,1,0,0,0,0,0,0,0,1]), # bare may-nov
                    np.array([1,1,1,1,1,1,1,0,0,0,0,1]), # bare aug-nov
                    np.ones(12), # never bare
                 ],
        'Cy0': linspace41(0,2*soil_default['Cy0']),
        'Ceq_frac': linspace41(0,0.5),
        'clay': linspace41(0,2*soil_default['clay']),
        'temp': [i*climate_default.temp for i in linspace41(0,2)],
        'rain': [i*climate_default.rain for i in linspace41(0,2)],
        'evap': [i*climate_default.evap for i in linspace41(0,2)],
        'fire_freq': np.array(range(1,21)),
        'litter_qty': linspace41(0,2),
        'litter_freq': np.array(range(1,21)),
        'fert_qty': linspace41(0,2),
        'fert_freq': np.array(range(1,21)),
        'fert_nitrogen': linspace41(0,0.036)
}


if __name__ == '__main__':
    
    # first cl arg is the type (tree, crop or soil)
    # second is the name of the param in the dict
    param_type = sys.argv[1]
    param_vary = sys.argv[2]

    # find right dicts to read
    if param_type == 'crop':
        default = crop_default
        vary = crop_vary
    elif param_type == 'tree':
        default = tree_default
        vary = tree_vary
    elif param_type == 'soil':
        default = soil_default
        vary = soil_vary
    else:
        print "Invalid param type (argument 1). Exitin..."
        sys.exit(1)
    
    try:
        # loop through desired range of param_vary. 
        # replace default[param_vary] with the i'th iteration
        # and pass to the model run
        emissions = []
        for i in vary[param_vary]:
            default[param_vary] = i
            if param_type == 'tree':
                emissions.append(main_cl.run_model(
                        default, crop_default, soil_default))
            elif param_type == 'crop':
                emissions.append(main_cl.run_model(
                        tree_default, default, soil_default))
            elif param_type == 'soil':
                emissions.append(main_cl.run_model(
                        tree_default, crop_default, default))
    
            print "%f,%f" % (i, emissions[-1])

    
    except KeyError:
        print "Invalid parameter to vary (argument 2). Exiting..."
        sys.exit(2)

