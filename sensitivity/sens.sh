#!/bin/bash

for f in tree/*; do
    n=$(basename $f .csv)
    echo "$n"
    ./sens.py tree "$n" $f
    echo "$n"
done
