#!/usr/bin/python

"""Main driver for SHAMBA command-line mode."""
import logging as log
import os
import sys

import matplotlib.pyplot as plt
import numpy as np

_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(_dir))

from shamba.model.climate import Climate
from shamba.model.soil_params import SoilParams

from shamba.model.crop_params import CropParams
from shamba.model.crop_model import CropModel

from shamba.model.tree_params import TreeParams
from shamba.model.tree_growth import TreeGrowth
from shamba.model.tree_model import TreeModel

from shamba.model.litter import LitterModel
from shamba.model.soil_model import InverseRothC, ForwardRothC
from shamba.model import cfg, emit, io_


def run_model(tree_params, crop_params, soil_params):
    """Main function where everything happens."""
    # Initial stuff
    
    cfg.SAV_DIR = '/home/mhughes6/shamba/shamba/sens_Enet/proj'
    cfg.INP_DIR = os.path.join(cfg.SAV_DIR, 'input')
    cfg.OUT_DIR = os.path.join(cfg.SAV_DIR, 'output')

    loc = soil_params['location']
    _climate_array = np.array([
            soil_params['temp'], soil_params['rain'], soil_params['evap']])
    climate = Climate(_climate_array) 
    
    # Set model run length from max tree age data available
    #cfg.N_YEARS = int(treeGrowth.age.max() + 20)
    cfg.N_YEARS = 50
    cfg.N_ACCT = cfg.N_YEARS
    emit.FIRE = np.zeros(cfg.N_YEARS)
    
    # Equilibrium solve
    _soil_params_dict = dict((k, soil_params[k]) for k in ['Cy0', 'clay'])
    soil = SoilParams(_soil_params_dict)
  
    # overwrite ceq
    soil.Ceq = (1+soil_params['Ceq_frac']) * soil_params['Cy0']
    
    # run inverse soil model

    invRoth = InverseRothC(soil,climate)
        
        
    # ----------
    # tree model
    # ----------
        
    # params
    _tree_nitrogen = np.array([
            tree_params['nitrogen_leaf'],
            tree_params['nitrogen_branch'], 
            tree_params['nitrogen_stem'], 
            tree_params['nitrogen_croot'], 
            tree_params['nitrogen_froot'],
    ])
    _tree_alloc = np.array([
            tree_params['alloc_leaf'],
            tree_params['alloc_branch'],
            tree_params['alloc_stem'],
            tree_params['alloc_croot'], 
            tree_params['alloc_froot']
    ])
    _tree_turnover = np.array([
            tree_params['turnover_leaf'],
            tree_params['turnover_branch'],
            tree_params['turnover_stem'],
            tree_params['turnover_croot'],
            tree_params['turnover_froot']
    ])

    _tree_params_dict = {
            'dens': tree_params['density'],
            'carbon': tree_params['carbon'],
            'rootToShoot': tree_params['root_to_shoot'],
            'nitrogen': _tree_nitrogen,
            'species': 'default',
    }
    tree_par = TreeParams(_tree_params_dict)
    
    _pool_params_dict = {
            'alloc': _tree_alloc,
            'turnover': _tree_turnover,
            'thinFrac': tree_params['thin_frac'],
            'mortFrac': tree_params['mort_frac'],
    }
    
    _growth_data = io_.read_csv('growth_test.csv')
    age = _growth_data[:,0]
    diam = _growth_data[:,1] * tree_params['growth']
    
    # growth model 
    growth = TreeGrowth(
            tree_par, {'age': age, 'diam': diam},
            allom=tree_params['allometric']
    )
    
    # run tree model
    thinning = np.zeros(cfg.N_YEARS+1)
    thinning[9] = tree_params['thin']
    mort = np.array((cfg.N_YEARS+1) * [tree_params['mort']])
    

    tree1 = TreeModel(
            tree_params=tree_par, tree_growth=growth,
            pool_params=_pool_params_dict,
            initialStandDens=tree_params['stand_dens'],
            thin=thinning, mort=mort
    ) 
    
    # ----------
    # Crop model
    # ----------
    
    # Baseline
    spp = 'maize'
    harvYield = crop_params['yield_baseline']
    harvFrac = 0.1
   
    crop_par_base = []
    crop_base = []
    ci = CropParams.from_species_name(spp)
    c = CropModel(ci, harvYield, harvFrac)
    crop_base.append(c)
    crop_par_base.append(ci)

    # Project 
    spp = 'maize'
    harvYield = crop_params['yield']
    harvFrac = crop_params['left_in_field']
    
    crop_par_proj = []
    crop_proj = []
    
    crop_params_dict = {
            'species': 'maize',
            'slope': crop_params['slope'],
            'intercept': crop_params['intercept'],
            'nitrogenBelow': crop_params['bgn'],
            'nitrogenAbove': crop_params['agn'],
            'carbonBelow': crop_params['bgc'],
            'carbonAbove': crop_params['agc'],
            'rootToShoot': crop_params['root_to_shoot']
    }
    ci = CropParams(crop_params_dict)
    
    c = CropModel(ci, harvYield, harvFrac)
    crop_par_proj.append(ci)
    crop_proj.append(c)

    # soil cover for project
    coverf = soil_params['cover']
   
    # --------------
    # SOIL MODEL
    # --------------
    # Solve to y=0
    forRoth = ForwardRothC(
            soil, climate, coverf, Ci=invRoth.eqC, 
            crop=crop_base, solveToValue=True)
    
    # Soil carbon for baseline and project
    roth_base = ForwardRothC(
            soil, climate, coverf, Ci=forRoth.SOC[-1], crop=crop_base)
    
    roth_proj = ForwardRothC(
            soil, climate, coverf, Ci=forRoth.SOC[-1],
            crop=crop_proj, tree=[tree1])

    
    # emissions
    emit_base = emit.Emission(
            forRothC=roth_base,
            crop=crop_base)
    emit_proj = emit.Emission(
            forRothC=roth_proj,
            crop=crop_proj,
            tree=[tree1],
            burnOff=False)

    # save sens stuff
    #filename = "emissions.csv"
    #emit_bymodel = np.column_stack(
    #        (emit_proj.emissions_soc, emit_proj.emissions_tree,
    #         emit_proj.emissions_fire, emit_proj.emissions_nitro)
    #)
    #labels = ["soil", "biomass", "fire", "nitrogen"]
    #io_.print_csv(
    #        filename, emit_bymodel, col_names=labels,
    #        print_column=True, print_years=True)

    #emit.Emission.save_(emit_proj, file=filename) 
     
    # ** for sensitivity stuff, only return the total project emission
    # let sub.py take care of actual printing
    
    #print "location: ",loc
    #climate.print_()
    #soil.print_()
    #growth.print_()
    #tree1.print_biomass()
    #tree1.print_balance()
    #forRoth.print_()
    #roth_base.print_()
    #roth_proj.print_()
    
    #print "\n\nEMISSIONS (t CO2)"
    #print "=================\n"
    #print "baseline    project"
    #emit_diff = emit_proj.emissions - emit_base.emissions
    #for i in range(len(emit_base.emissions)):
    #    print emit_base.emissions[i],emit_proj.emissions[i],emit_diff[i]

    #print "\nTotal difference: ",sum(emit_diff), " t CO2 ha^-1"
    #print "Average difference: ", np.mean(emit_diff)
      
    # Save stuff 
    #climate.save_()
    #soil.save_()
    #growth.save_()
    #tree_par.save_()
    #tree1.save_()
    #i = 1
    #for i in range(len(crop_base)):
    #    crop_base[i].save_("crop_model_base_"+str(i)+".csv")
    #    crop_par_base[i].save_("crop_params_base_"+str(i)+".csv")
    #    crop_proj[i].save_("crop_model_proj_"+str(i)+".csv")
    #    crop_par_proj[i].save_("crop_params_proj_"+str(i)+".csv")

    #invRoth.save_()
    #forRoth.save_()
    #roth_base.save_('soil_model_base.csv')
    #roth_proj.save_('soil_model_proj.csv')
    #emit_proj.save_(emit_base, emit_proj)

    # Plot stuff
    #growth.plot_(saveName='growthFits.png')
    #tree1.plot_biomass()
    
    #tree1.plot_balance(saveName='massBalance.png')
    #forRoth.plot_(legendStr='initialisation')
    #roth_base.plot_(legendStr='baseline')
    #roth_proj.plot_(legendStr='intervention', saveName='soilModel.png')
    #emit_diff = emit_proj.emissions - emit_base.emissions
    #emit_base.plot_(legendStr='baseline')
    #emit_proj.plot_(legendStr='project')
    #emit.Emission.ax.plot(emit_diff, label='difference')
    #emit.Emission.ax.legend(loc='best')
    #plt.savefig(os.path.join(cfg.OUT_DIR, 'emissions.png'))
    
    #emissions_soc = np.column_stack(
    #(emit_proj.emissions_soc,
    #         emit_base.emissions_soc,
    #         emit_proj.emissions_soc - emit_base.emissions_soc
    #        )
    #)
    #emissions_tree = np.column_stack(
    #        (emit_proj.emissions_tree,
    #         np.zeros(50),
    #         emit_proj.emissions_tree)
    #)
    #emissions_fire = np.column_stack(
    #        (emit_proj.emissions_fire,
    #         emit_base.emissions_fire,
    #         emit_proj.emissions_fire - emit_base.emissions_fire)
    #)
    #emissions_nitro = np.column_stack(
    #        (emit_proj.emissions_nitro,
    #         emit_base.emissions_nitro,
    #         emit_proj.emissions_nitro - emit_base.emissions_nitro)
    #)
    #emit_all = np.column_stack(
    #        (emissions_soc, emissions_tree, emissions_fire, emissions_nitro)
    #)
    
    #print sum(emit_proj.emissions_soc)
    #print sum(emit_base.emissions_soc)
    #print sum(emit_proj.emissions_tree)
    #print sum(emit_proj.emissions_fire)
    #print sum(emit_base.emissions_fire)
    #print sum(emit_proj.emissions_nitro)
    #print sum(emit_base.emissions_nitro)
    #print sum(emit_proj.emissions_nitro) - sum(emit_base.emissions_nitro)
    #emit_proj.save_(emit_base, emit_proj=emit_proj)
    #io_.print_csv(
    #        'emissions_bymodel.csv', emit_all, 
    #        col_names=[
    #                'proj_soc', 'base_soc', 'diff_soc', 
    #                'proj_tree', 'base_tree', 'diff_tree',
    #                'diff_tree'proj_fire', 'base_fire', 'diff_fire',
    #                'proj_nitro', 'base_nitro', 'diff_nitro'],
    #       print_years=True)
    
    # emissions for various sd
    #filename = 'emissions_standdens'+str(tree_params['stand_dens'])+'.csv'
    emit_diff = emit_proj.emissions - emit_base.emissions
    #io_.print_csv(
    #        filename, emit_diff, col_names=['emissions'], print_years=True)

    plt.show()
    
    return sum(emit_diff)
