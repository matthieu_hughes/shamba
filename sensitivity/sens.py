#!/usr/bin/python

import numpy as np
import sys
import sub


if __name__ == '__main__':
    
    # 1st cl arg is the type (crop, tree or soil)
    # 2nd cl arg is the name of the param to calculate the sensitivity of
    # 3rd cl arg is the name of the csv file with cols=(param,emissions)
    param_type = sys.argv[1]
    param_vary = sys.argv[2]
    filename = sys.argv[3]
    
    # read and store data in file
    param_data, response_data = np.loadtxt(
            filename, delimiter=',', unpack=True)
    
    # find default param and response
    default_response = -136.743576
     
    if param_type == 'crop':
        default_param = sub.crop_default[param_vary]
    elif param_type == 'tree':
        default_param = sub.tree_default[param_vary]
    elif param_type == 'soil':
        default_param = sub.soil_default[param_vary]
    else:
        print "Invalid argument for param_type. Exiting."
        sys.exit(1)

    
    # Calculate sens param (S) for each param value in param_data
    x = param_data / default_param
    #print x
    sens = ((response_data-default_response)/abs(default_response)) / \
           ((param_data-default_param)/abs(default_param))
    #print sens

    # print out to a new file
    filename = filename.split(".csv")[0] + "_sens.csv"
    
    data = np.column_stack((param_data, response_data, x, sens))
    np.savetxt(filename, data, fmt="%.5f", delimiter=",")


