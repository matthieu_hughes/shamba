#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt


def load_(filename):
    return np.loadtxt(filename, delimiter=',', skiprows=1, usecols=[1])

sd100 = (load_('emissions_standdens100.0.csv'), "100 trees / ha")
sd200 = (load_('emissions_standdens200.0.csv'), "200 trees / ha")
sd300 = (load_('emissions_standdens300.0.csv'), "300 trees / ha")
sd400 = (load_('emissions_standdens400.0.csv'), "400 trees / ha")
sd500 = (load_('emissions_standdens500.0.csv'), "500 trees / ha")

if __name__ == '__main__':
    fig = plt.figure()
    fig.canvas.set_window_title("Emissions")
    ax = fig.add_subplot(1,1,1)
    ax.set_title("Emissions vs time")
    ax.set_xlabel("Time (years)")
    ax.set_ylabel("Emissions (t CO_2 / ha)")

    for sd in [sd100, sd200, sd300, sd400, sd500]:
        ax.plot(sd[0], label=sd[1])

    ax.legend(loc='best')
    plt.show()

