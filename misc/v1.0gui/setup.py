#!/usr/bin/env python

from distutils.core import setup, Command
from distutils.dep_util import newer, newer_group
from distutils.command.build import build as _build_orig
from distutils import log
import os,os.path
import sys
from glob import glob

df = []
for f in ['data.config','MAA_satellite.tfw','MAA_satellite.tif','IMG_0223.JPG','IMG_0340.JPG','IMG_0325.JPG']:
    df.append(os.path.join('data',f))
data_files = [('share/shamba',df)]
df = []
for f in ['baseline.data','data.config','project.data']:
    df.append(os.path.join('data','data1',f))
data_files += [('share/shamba/data1',df)]

extra = {}

# check if we are on windows
if os.name == 'nt':
    import py2exe
    from glob import glob
    data_files.append(("Microsoft.VC90.CRT", glob(r'C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC\redist\x86\Microsoft.VC90.CRT\*.*')))
    sys.path.append("C:\\Program Files (x86)\\Microsoft Visual Studio 9.0\\VC\\redist\\x86\\Microsoft.VC90.CRT")

    extra['windows'] = [{"script":"shamba.pyw"}]
    extra['options'] = {
        "py2exe" : {
            "skip_archive" : 0,

            # Don't pull in all this MFC stuff used by the makepy UI.
            "excludes" : "pywin,pywin.dialogs,pywin.dialogs.list"
            ",setup,distutils",  # required only for in-place use
            "includes" : ["sip"],
            "optimize" : 1
            }
        }

class build_qt(Command):
    """lifted from https://bitbucket.org/tortoisehg/thg/src/11df59e9cfbe/setup.py"""
    description = "build PyQt GUIs (.ui) and resources (.qrc)"
    user_options = [('force', 'f', 'forcibly compile everything'
                     ' (ignore file timestamps)'),
                    ('frozen', None, 'include resources for frozen exe')]
    boolean_options = ('force', 'frozen')

    def initialize_options(self):
        self.force = None
        self.frozen = False

    def finalize_options(self):
        self.set_undefined_options('build', ('force', 'force'))

    def compile_ui(self, ui_file, py_file=None):
        # Search for pyuic4 in python bin dir, then in the $Path.
        if py_file is None:
            py_file = os.path.splitext(ui_file)[0] + "_ui.py"
        if not(self.force or newer(ui_file, py_file)):
            return
        try:
            from PyQt4 import uic
            fp = open(py_file, 'w')
            uic.compileUi(ui_file, fp)
            fp.close()
            log.info('compiled %s into %s' % (ui_file, py_file))
        except Exception, e:
            self.warn('Unable to compile user interface %s: %s' % (py_file, e))
            if not os.path.exists(py_file) or not file(py_file).read():
                raise SystemExit(1)
            return

    def compile_rc(self, qrc_file, py_file=None):
        # Search for pyuic4 in python bin dir, then in the $Path.
        if py_file is None:
            py_file = os.path.splitext(qrc_file)[0] + "_rc.py"
        if not(self.force or newer(qrc_file, py_file)):
            return
        import PyQt4
        origpath = os.getenv('PATH')
        path = origpath.split(os.pathsep)
        pyqtfolder = os.path.dirname(PyQt4.__file__)
        path.append(os.path.join(pyqtfolder, 'bin'))
        os.putenv('PATH', os.pathsep.join(path))
        if os.system('pyrcc4 "%s" -o "%s"' % (qrc_file, py_file)) > 0:
            self.warn("Unable to generate python module %s for resource file %s"
                      % (py_file, qrc_file))
            if not exists(py_file) or not file(py_file).read():
                raise SystemExit(1)
        else:
            log.info('compiled %s into %s' % (qrc_file, py_file))
        os.putenv('PATH', origpath)

    def _generate_qrc(self, qrc_file, srcfiles, prefix):
        basedir = os.path.dirname(qrc_file)
        f = open(qrc_file, 'w')
        try:
            f.write('<!DOCTYPE RCC><RCC version="1.0">\n')
            f.write('  <qresource prefix="%s">\n' % cgi.escape(prefix))
            for e in srcfiles:
                relpath = e[len(basedir) + 1:]
                f.write('    <file>%s</file>\n'
                        % cgi.escape(relpath.replace(os.path.sep, '/')))
            f.write('  </qresource>\n')
            f.write('</RCC>\n')
        finally:
            f.close()

    def build_rc(self, py_file, basedir, prefix='/'):
        """Generate compiled resource including any files under basedir"""
        # For details, see http://doc.qt.nokia.com/latest/resources.html
        qrc_file = os.path.join(basedir, '%s.qrc' % os.path.basename(basedir))
        srcfiles = [os.path.join(root, e)
                    for root, _dirs, files in os.walk(basedir) for e in files]
        # NOTE: Here we cannot detect deleted files. In such case, we need
        # to remove .qrc manually.
        if not (self.force or newer_group(srcfiles, py_file)):
            return
        try:
            self._generate_qrc(qrc_file, srcfiles, prefix)
            self.compile_rc(qrc_file, py_file)
        finally:
            os.unlink(qrc_file)

    def run(self):
        basepath = os.path.join(os.path.dirname(__file__), 'shamba')
        for dirpath, _, filenames in os.walk(basepath):
            for filename in filenames:
                if filename.endswith('.ui'):
                    self.compile_ui(os.path.join(dirpath, filename))
                elif filename.endswith('.qrc'):
                    self.compile_rc(os.path.join(dirpath, filename))

class build(_build_orig):
    sub_commands = [
        ('build_qt', None),
        ] + _build_orig.sub_commands

cmdclass = {
        'build': build,
        'build_qt': build_qt
        }

setup(name='shamba',
      version='0.7',
      description = "shamba data browser",
      author = "Magnus Hagdorn",
      author_email = "magnus.hagdorn@ed.ac.uk",
      packages = ['shamba'],
      scripts=['shamba.pyw','parseOutput.py'],
      data_files=data_files,
      cmdclass = cmdclass,
      **extra
)
