#!/bin/env python

import sys
from PyQt4 import QtCore, QtGui
import optparse
import shamba

parser = optparse.OptionParser()
parser.add_option("-d","--data-config",metavar="FILE",help="load data configuration from FILE")
(options, args) = parser.parse_args()

app = QtGui.QApplication(sys.argv)

# setup splash screen
splash_pix = QtGui.QPixmap(':/splash.jpg')
splash = QtGui.QSplashScreen(splash_pix)
splash.setMask(splash_pix.mask())
splash.show()
splash.showMessage('Starting...')
app.processEvents()


bmData = shamba.BasemapSetup()
locations = shamba.Locations()

form = shamba.DisclaimerDialog()
form.exec_()

if options.data_config!=None:
    cname = options.data_config
else:
    cname = str(QtGui.QFileDialog.getOpenFileName(None,"Open Configuration"))


splash.showMessage('Loading Data')
bmData.loadCfg(cname)
locations.loadCfg(cname)

splash.showMessage('Initialising Application')
myapp = shamba.ShambaMain(basemapData=bmData,locations=locations)
myapp.show()
splash.finish(myapp)
sys.exit(app.exec_())
