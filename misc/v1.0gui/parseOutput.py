#!/bin/env python

import sys

def checkBeginning(line,phrase):
    return line[0:len(phrase)] == phrase
        
if len(sys.argv)!=3:
    print 'Usage: %s INFILE OUTFILE'%(sys.argv[0])
    sys.exit(1)


inname = sys.argv[1]
outname = sys.argv[2]

fields = ['DPM','RPM','BIO','HUM','IOM','Total']

data  = {}

thisYear = None
for line in open(inname,'r').readlines():
    line = line.strip()
    token = line.split()
    
    if checkBeginning(line,'After year'):
        thisYear = int(token[2])
        data[thisYear] = {}
    if thisYear != None:
        if checkBeginning(line,"Radiocarbon activity scaling factor"):
            data[thisYear]['scaling_factor'] = float(token[5])
        for f in fields:
            if checkBeginning(line,f):
                data[thisYear][f] = (float(token[1]), float(token[2]), float(token[3]))
        if checkBeginning(line,'The accumulated CO2 lost to the atmosphere is'):
            data[thisYear]['accumulated_co2_loss'] = float(token[8])

years = data.keys()
years.sort()

out = open(outname,'w')

for y in years:
    out.write("%f %f\n"%(y,data[y]['Total'][0]))
out.close()
