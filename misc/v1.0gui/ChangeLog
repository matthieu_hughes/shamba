2012-07-13  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba.pyw: start early writing to splash
 * shamba/disclaimer.py: load correct resource file
 * shamba/splash.xcf: add logos
 * shamba/splash.jpg: regenerated

2012-07-13  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * MANIFEST.in: add new file
 * shamba/resources.qrc: add variable config to resource file
 * shamba/shamba.py: load variable configuration file from resourcecs and use
   it to populate various labels
 * shamba/variables.config: describe variables

2012-07-13  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/plot.py: minor tweaks of labels

2012-06-28  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * setup.py: bump version
 * shamba/map.py: add method to save image
 * shamba/report.py: add class to handle report generation
 * shamba/shamba.py: hook up report generator

2012-06-28  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * setup.py: bump version
 * shamba/plot.py: tweaks to total display
 * shamba/shamba.ui: increase window size

2012-06-28  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/shamba.ui: change tab titles

2012-06-28  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/plot.py: illustrate removal/emission

2012-06-28  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/shamba.py: fix names

2012-06-27  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * setup.py: bump version

2012-06-27  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * MANIFEST.in: add new file
 * shamba.pyw: pop up disclaimer dialog
 * shamba/__init__.py: import disclaimer dialog
 * shamba/disclaimer.py: disclaimer dialog implementation
 * shamba/disclaimer.ui: disclaimer dialog ui
 * shamba/shamba.ui: rename window

2012-06-25  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * MANIFEST.in: add some more files
 * setup.py: increase release number

2012-06-25  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/plot.py: display totals

2012-06-25  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/shamba.py: reset plots when switching to a new site

2012-06-25  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * setup.py: add code to compile resource files
 * shamba.pyw: show splash screen
 * shamba/__init__.py: load resources
 * shamba/resources.qrc: add resource file
 * shamba/splash.xcf: splash screen

2012-06-22  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * parseOutput.py: add program to parse model output files
 * setup.py: distribute new file, bump version number
 * data/CU01.263: add example data file

2012-06-22  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * MANIFEST.in: add some more files
 * setup.py: install the images
 * data/data.config: specify images to load
 * shamba/data.py: load images if specified
 * shamba/shamba.py: optionally display images
 * shamba/shamba.ui: add image display

2012-06-21  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/data.py: do not load data for the year zero
 * shamba/plot.py: fix units

2012-06-21  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * MANIFEST.in: add datafiles
 * setup.py: add datafiles; rename main script to keep windows happy
 * shamba.py renamed to shamba.pyw to keep windows happy

2012-06-21  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba.py: use file dialog to specify config file when none is specified
   on the command line

2012-06-21  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/plot.py: add method to save figure
 * shamba/shamba.py: hook up save figure button
 * shamba/shamba.ui: add a save figure button

2012-06-21  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba/__init__.py: setup matplotlib backend only once
 * shamba/map.py: matplotlib backend is setup in __init__.py
 * shamba/plot.py: implement model plot widget
 * shamba/shamba.py: hook up model plot
 * shamba/shamba.ui: add plot tab

2012-06-21  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * data/data1/baseline.data: add model output
 * data/data1/project.data: ditto
 * data/data1/data.config: refer to model output
 * shamba/data.py: add class to load model output
 * shamba/shamba.py: fill model table manually, hook up checkbox to select
   model for plotting
 * shamba/shamba.ui: no rows/columns in models table widget

2012-06-20  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * data/data.config: add real project descriptions
 * data/data1/data.config: add model descriptions for one of the projects
 * shamba/data.py: add class to handle model descritpions
 * shamba/shamba.py: implement model GUI
 * shamba/shamba.ui: design model tab

2012-06-19  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * shamba.py: load locations file
 * data/data.config: add an example lcoation
 * shamba/data.py: add class to load locations
 * shamba/map.py: add method to plot all locations
 * shamba/shamba.py: add method to display location info
 * shamba/shamba.ui: display location info

2012-06-07  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * MANIFEST.in: add new file
 * shamba.py: move opening of config file to data handler
 * data/MAA_satellite.tif: add satellite image
 * data/data.config: setup projection for UTM36S, load boundary from
   geotiff
 * shamba/data.py: get bounding box from geotiff
 * shamba/map.py: display satellite map if geotiff specified

2012-06-07  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * MANIFEST.in: add data file
 * setup.py: handle py2exe builds
 * shamba.py: load map setup from data file
 * data/data.config: setup map area
 * shamba/data.py: handle loading of map setup
 * shamba/map.py: draw country boundaries

2012-06-04  Magnus Hagdorn  <magnus.hagdorn@geos.ed.ac.uk>
 * initial import
 * add python distutils support

