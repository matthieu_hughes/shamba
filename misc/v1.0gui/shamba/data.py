
__all__ = ['BasemapSetup','Locations','Datasets']

from osgeo import gdal
import ConfigParser, os.path
import math, numpy

LocationVars = {'Country':str,'District':str,'SOC':float,'Clay':float,'MAT':float,'MAR':float}
BaselineVars = {'Bcrop':str,'Byield':float,'BCHI':float,'BRR':str,'BFB':str,'Btil':str}
ProjectsVars = {'Pcrop':str,'Ptree':str,'Pyield':float,'Pylo':float,'Pyhi':float,'PRR':str,'PFB':str,'Ptil':str}
BaselineData = {'Year':0,'B_em':1}
ProjectsData = {'Year':0,'P_em_med':1,'P_em_low':2,'P_em_high':3}

class BasemapSetup(object):
    def __init__(self):
        self.geotiff = None

        # geographic coordinates of the region
        self.llcrnrlon=-10.5
        self.llcrnrlat=49.5
        self.urcrnrlon=3.5
        self.urcrnrlat=59.5

        # the projection
        self.projection='tmerc'
        self.lon_0 = -4.36
        self.lat_0=54.7

        # the coordinate system
        self.first_parallel = -40.
        self.last_parallel = 59.
        self.delta_parallel = 2.
        self.first_meridian = -20.
        self.last_meridian = 19.
        self.delta_meridian = 2.

    def loadCfg(self,cname):
        cfg = ConfigParser.ConfigParser()
        cfg.read(cname)

        sname = 'basemap'
        if cfg.has_section(sname):
            have_corners = False

            # sort out the projection
            if cfg.has_option(sname,'projection'):
                self.projection = cfg.get(sname,'projection')
            else:
                raise RuntimeError, 'missing projection'
            if self.projection == 'tmerc':
                for o in ['lon_0','lat_0']:
                    if not cfg.has_option(sname,o):
                        raise RuntimeError, 'missing projection parameter, %s'%o
                    setattr(self,o,cfg.getfloat(sname,o))

            # load geotiff
            if cfg.has_option(sname,'geotiff'):
                self.geotiff = os.path.join(os.path.dirname(cname),cfg.get(sname,'geotiff'))
                ds = gdal.Open(self.geotiff)
                # get the corners
                gt = ds.GetGeoTransform()
                width = ds.RasterXSize
                height = ds.RasterYSize
                minx = gt[0]
                miny = gt[3] + width*gt[4] + height*gt[5] 
                maxx = gt[0] + width*gt[1] + height*gt[2]
                maxy = gt[3] 
                
                self.llcrnrlon = minx
                self.llcrnrlat = miny
                self.urcrnrlon = maxx
                self.urcrnrlat = maxy
            else:
                # get the coordinates of the corners
                for o in ['llcrnrlon','llcrnrlat','urcrnrlon','urcrnrlat']:
                    if not cfg.has_option(sname,o):
                        raise RuntimeError, 'missing coordinate, %s'%o
                    setattr(self,o,cfg.getfloat(sname,o))


            # specify the coordinate system
            for o in ['delta_parallel','delta_meridian']:
                if cfg.has_option(sname,o):
                    setattr(self,o,cfg.getfloat(sname,o))
            if cfg.has_option(sname,'first_parallel'):
                self.first_parallel = cfg.getfloat(sname,'first_parallel')
            else:
                self.first_parallel = math.floor(self.llcrnrlat-self.delta_parallel)
            if cfg.has_option(sname,'last_parallel'):
                self.last_parallel = cfg.getfloat(sname,'last_parallel')
            else:
                self.last_parallel = math.floor(self.urcrnrlat+self.delta_parallel)
            if cfg.has_option(sname,'first_meridian'):
                self.first_meridian = cfg.getfloat(sname,'first_meridian')
            else:
                self.first_meridian = math.floor(self.llcrnrlon-self.delta_meridian)
            if cfg.has_option(sname,'last_meridian'):
                self.last_meridian = cfg.getfloat(sname,'last_meridian')
            else:
                self.last_meridian = math.floor(self.urcrnrlon+self.delta_meridian)
            

class Locations(object):
    significants = 4
    ignore_sections = ['basemap']

    def __init__(self):
        self.locations = []
        self.data = []

    def loadCfg(self,cname):
        cfg = ConfigParser.ConfigParser()
        cfg.read(cname)

        for s in cfg.sections():
            if s not in self.ignore_sections:
                loc = {}
                for o in LocationVars.keys():
                    if not cfg.has_option(s,o):
                        raise RuntimeError, 'Location %s is missing option %s'%(s,o)
                    loc[o] = LocationVars[o](cfg.get(s,o))
                loc['Name'] = s
                if not cfg.has_option(s,'Coordinates'):
                    raise RuntimeError, 'Location %s is missing option Coordinates'%(s)
                coords = cfg.get(s,'Coordinates').split(',')
                try:
                    coords = (float(coords[0]),float(coords[1]))
                except:
                    raise RuntimeError, 'Cannot parse Coordinates %s'%cfg.get(s,'Coordinates')
                loc['Coordinates'] = coords

                if not cfg.has_option(s,'data'):
                    raise RuntimeError, 'Location %s is missing option data'%(s)
                loc['data'] = os.path.join(os.path.dirname(cname),cfg.get(s,'data'))

                if cfg.has_option(s,'img'):
                    loc['img'] = os.path.join(os.path.dirname(cname),cfg.get(s,'img'))

                self.data.append(loc)
                self.locations.append(numpy.array(coords))
                
class Datasets(object):

    def __init__(self):
        self.datasets = {}

    def loadCfg(self,cname):
        cfg = ConfigParser.ConfigParser()
        cfg.read(cname)
        
        for s in cfg.sections():
            ds = {}
            if cfg.has_option(s,'Bcrop'):
                options = BaselineVars
                dcols = BaselineData
            elif cfg.has_option(s,'Pcrop'):
                options = ProjectsVars
                dcols = ProjectsData
            else:
                raise RuntimeError, 'Unknown data set configuration %s'%s
            for o in options:
                if not cfg.has_option(s,o):
                    raise RuntimeError, 'Data set %s is missing option %s'%(s,o)
                ds[o] = options[o](cfg.get(s,o))
            # set name of data file
            if cfg.has_option(s,'data'):
                ds['data'] = ModelData(dcols,os.path.join(os.path.dirname(cname),cfg.get(s,'data')))
            else:
                ds['data'] = None
            
            self.datasets[s] = ds

class ModelData(object):
    def __init__(self,cols,fname):
        self.__cols = cols
        self.__fname = fname
        self.__data = None

    def getData(self):
        if self.__data == None:
            self.readData()
        return self.__data
    data = property(getData)

    def readData(self):
        self.__data = {}
        for c in self.__cols:
            self.__data[c] = []
        i = 0
        for line in open(self.__fname,'r').readlines():
            line = line.strip()
            if len(line) == 0:
                continue
            line = line.split()
            if len(line) != len(self.__cols):
                raise RuntimeError, 'Error on line %d of file %s, expected %d cols got %d'%(i,self.__fname,len(self.__cols),len(line))

            if float(line[self.__cols['Year']]) < 1.0:
                continue

            for c in self.__cols:
                j = self.__cols[c]
                try:
                    v = float(line[j])
                except:
                    raise RuntimeError, 'Error, parsing %dth value %s on line %d of file %s'%(j,line[j],i,self.__fname)
                self.__data[c].append(v)
            i+=1
        for c in self.__cols:
            self.__data[c] = numpy.array(self.__data[c])


if __name__ == "__main__":
    import sys
    
    locs = Locations()
    locs.loadCfg(sys.argv[1])
    print locs.locations.keys()
    print locs.locations
