import pylab, numpy

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.image
from matplotlib.figure import Figure
from mpl_toolkits.basemap import Basemap
from matplotlib.patches import Polygon
from matplotlib.lines import Line2D

from PyQt4 import QtCore,QtGui

COLOURS = 'bgrcmy'

class PlotWidget(FigureCanvas):
    """a QtWidget and FigureCanvasAgg that displays the model plots"""

    def __init__(self, parent=None, name=None, width=5, height=4, dpi=100, bgcolor=None):
        self.fig = Figure(figsize=(width, height), dpi=dpi, facecolor=bgcolor, edgecolor=bgcolor)
        self.axes = self.fig.add_axes([0.125,0.1,0.6,0.8]) #self.fig.add_subplot(111)

        self.have_range = []

        self.axes.set_xlabel('Time [yr]')
        self.axes.set_ylabel('Greenhouse Gas \nEmissions/Removals [tCO2/ha/yr]',multialignment='center')

        self.axes.axhline(y=0.,ls=':',color='k')
        self.axes.text(0, 0,'Emission',va='bottom')
        self.axes.text(0, 0,'Removal',va='top')
        self.emission_patch = None
        self.removal_patch = None
        self.updateShading()

        self.totalsBox = self.fig.text(0.75,0.1,"")

        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        self.setSizePolicy(QtGui.QSizePolicy.Expanding,
                           QtGui.QSizePolicy.Expanding)
        self.updateGeometry()

        self.plots = {}
        self.totals = {}

    def addPlot(self,plotID,data,model):
        plots = []
        if 'B_em' in data.data:
            plots += self.axes.plot(data.data['Year'],data.data['B_em'],color=COLOURS[plotID],label=model)
            totals = (model,numpy.sum(data.data['B_em']),)
        if 'P_em_med' in data.data:
            self.have_range.append(plotID)
            plots += self.axes.plot(data.data['Year'],data.data['P_em_med'],color=COLOURS[plotID],label=model)
            plots += self.axes.plot(data.data['Year'],data.data['P_em_low'],color=COLOURS[plotID],ls='--')
            plots += self.axes.plot(data.data['Year'],data.data['P_em_high'],color=COLOURS[plotID],ls='--')
            totals = (model,numpy.sum(data.data['P_em_med']),numpy.sum(data.data['P_em_low']),numpy.sum(data.data['P_em_high']))
        self.axes.autoscale(enable=True)
        self.axes.relim()
        self.plots[plotID] = plots
        self.totals[plotID] = totals
        self.updateLegend()
        self.updateTotals()
        self.updateShading()
        self.draw()

    def updateShading(self):
        # update vertical limits
        xlim = self.axes.get_xlim()
        ylim = self.axes.get_ylim()
        ylim = (min(ylim[0],-1),max(ylim[1],1))
        self.axes.set_ylim(ylim)
        upper = [(xlim[0],0),(xlim[1],0),(xlim[1],ylim[1]),(xlim[0],ylim[1])]
        lower = [(xlim[0],ylim[0]),(xlim[1],ylim[0]),(xlim[1],0),(xlim[0],0)]
        # shade positive and negative values
        if self.emission_patch == None:
            self.emission_patch = Polygon(upper,facecolor='r',alpha=0.3,fill=True,edgecolor=None,zorder=-1000)
            self.axes.add_patch(self.emission_patch)
        else:
            self.emission_patch.set_xy(upper)
        if self.removal_patch == None:
            self.removal_patch = Polygon(lower,facecolor='b',alpha=0.3,fill=True,edgecolor=None,zorder=-1000)
            self.axes.add_patch(self.removal_patch)
        else:
            self.removal_patch.set_xy(lower)
        #self.removal_patch = None


    def updateTotals(self):
        models = {}
        for c in self.totals:
            models[self.totals[c][0]] = self.totals[c][1:]
        ms = models.keys()
        ms.sort()
        years =  self.axes.dataLim.get_points()[1,0]
        outstr = "Total Emissions/Removals\nover %d years [tCO2e/ha]:\n"%(years)
        for m in ms:
            outstr+='%s '%m
            if len(models[m])==1:
                outstr+='%.0f\n'%models[m][0]
            else:
                outstr+='%.0f (%.0f, %.0f)\n'%models[m]
        if len(self.totals)>1 and 0 in self.totals:
            outstr+="\nNet impact [tCO2e]:\n"
            for c in self.totals:
                if c == 0: 
                    continue
                outstr+="%s %.0f (%.0f, %.0f)\n"%(self.totals[c][0],self.totals[c][1]-self.totals[0][1],
                                                self.totals[c][2]-self.totals[0][1],self.totals[c][3]-self.totals[0][1])
        outstr+="\n\nbrackets indicate\nmodelled range\n"
                    

        self.totalsBox.set_text(outstr)


    def removePlot(self,plotID):
        if plotID in self.plots:
            for p in self.plots[plotID]:
                p.remove()
            self.draw()
            del self.totals[plotID]
            del self.plots[plotID]
            if plotID in self.have_range:
                self.have_range.remove(plotID)
            self.updateLegend()
            self.updateTotals()
        

    def updateLegend(self):
        if len(self.plots.keys())>0:
            handles, labels = self.axes.get_legend_handles_labels()
            if len(self.have_range) > 0:
                l = Line2D([0,1],[0,1],linestyle="--",color='k')
                handles.append(l)
                labels.append("range")
            self.axes.legend(handles,labels,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        else:
            self.axes.legend_ = None

    def saveFigure(self,fname):
        self.fig.savefig(fname)
                

    def sizeHint(self):
        w = self.fig.get_figwidth()
        h = self.fig.get_figheight()
        return QtCore.QSize(w, h)

    def minimumSizeHint(self):
        return QtCore.QSize(10, 10)
