#!/bin/env python

__all__ = ['ShambaMain']

import sys
import ConfigParser,StringIO
from PyQt4 import QtCore, QtGui
from shamba_ui import Ui_MainWindow
from data import BasemapSetup, Datasets
from report import ShambaReport
import numpy

class VariableCfg(object):
    def __init__(self,fp):
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.readfp(fp)

    def getLabel(self,var):
        label = var
        if self.cfg.has_section(var):
            if self.cfg.has_option(var,'long_name'):
                label = self.cfg.get(var,'long_name')
        return label

    def getUnits(self,var):
        units = ''
        if self.cfg.has_section(var):
            if self.cfg.has_option(var,'units'):
                units = self.cfg.get(var,'units')
        return units

    def getLabelAndUnits(self,var):
        label = self.getLabel(var)
        units = self.getUnits(var)
        if len(units)>0:
            if len(label)>0:
                label += ' '
            label += '[%s]'%units
        return label


class ShambaMain(QtGui.QMainWindow):
    ROWS = ['Model','Crop','Agro','Yield','Ylo','Yhi','CHI','RR','FB','Til','Plot']
    LOC_LABELS = ['Country','District','SOC','Clay','MAT','MAR']
    def __init__(self, parent=None,basemapData=None,locations=None):

        self.locations = locations
        self.locationID = 0

        self.datasets = Datasets()

        varCfg = QtCore.QFile(":/variables.config")
        if not varCfg.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            raise RuntimeError
        self.varCfg = VariableCfg(StringIO.StringIO(varCfg.readAll()))

        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # set location tool tips
        for l in self.LOC_LABELS:
            label = self.varCfg.getLabelAndUnits(l)
            if len(label)>0:
                getattr(self.ui,'%sLabel'%l).setToolTip(label)

        # set bounding box
        self.ui.longitudeEntry.setMinimum(basemapData.llcrnrlon)
        self.ui.longitudeEntry.setMaximum(basemapData.urcrnrlon)
        self.ui.latitudeEntry.setMinimum(basemapData.llcrnrlat)
        self.ui.latitudeEntry.setMaximum(basemapData.urcrnrlat)
        self.ui.mapWidget.drawBasemap(basemapData)
        # hook up map callback
        self.ui.mapWidget.pickCallback = self.mapPick
        if locations!=None:
            self.ui.mapWidget.plotLocations(self.locations)
        self.setLocation(0)

        #setup model table
        self.ui.models.horizontalHeader().setVisible(True)
        self.ui.models.setRowCount(len(self.ROWS))
        self.ui.models.setAlternatingRowColors(True)
        self.ui.models.horizontalHeader().setCascadingSectionResizes(True)
        self.ui.models.horizontalHeader().setDefaultSectionSize(200)
        self.ui.models.horizontalHeader().setSortIndicatorShown(False)
        self.ui.models.verticalHeader().setVisible(True)
        for i in range(len(self.ROWS)):
            item = QtGui.QTableWidgetItem()
            self.ui.models.setVerticalHeaderItem(i, item)
            label = self.varCfg.getLabel(self.ROWS[i])
            self.ui.models.verticalHeaderItem(i).setText(label)
            units = self.varCfg.getUnits(self.ROWS[i])
            if len(units)>0:
                self.ui.models.verticalHeaderItem(i).setToolTip(units)
        self.resetModels()

    def resetModels(self):
        if hasattr(self,"modelPlots"):
            for t in self.modelPlots:
                t.setCheckState(0)
        self.ui.models.setColumnCount(0)
        self.modelSelectors = []
        self.modelPlots = []

    def addModel(self):
        models = self.datasets.datasets.keys()
        models.sort()
        comboBox = QtGui.QComboBox()
        comboBox.addItems(models)
        self.modelSelectors.append(comboBox)

        checkBox = QtGui.QCheckBox()
        self.modelPlots.append(checkBox)

        column = self.ui.models.columnCount()

        self.ui.models.setColumnCount(column+1)
        self.ui.models.setCellWidget(0,column,comboBox)
        self.ui.models.setCellWidget(len(self.ROWS)-1,column,checkBox)
        # fill in the remaining rows
        for i in range(1,len(self.ROWS)-1):
            item = QtGui.QTableWidgetItem()
            item.setFlags(QtCore.Qt.ItemIsSelectable|QtCore.Qt.ItemIsEnabled)
            self.ui.models.setItem(i, column, item)

        # hook up call back
        self.connect(comboBox,QtCore.SIGNAL("currentIndexChanged(int)"),lambda state : self.selectModel(column,state))        
        # and fill in model parameters
        self.selectModel(column,0)

        self.connect(checkBox,QtCore.SIGNAL("stateChanged(int)"),lambda state : self.plotModel(column,state))

    def plotModel(self,column,state):
        model = str(self.modelSelectors[column].currentText())
        data = self.datasets.datasets[model]['data']
        if state == 2:
            self.ui.plotWidget.addPlot(column,data,model)
        elif state == 0:
            self.ui.plotWidget.removePlot(column)

    def selectModel(self,column,idx):
        model = str(self.modelSelectors[column].currentText())
        # reset plot tick box
        self.modelPlots[column].setCheckState(0)
        if model in self.datasets.datasets.keys():
            data = self.datasets.datasets[model]
            if 'Bcrop' in data:
                self.ui.models.item(1,column).setText(data['Bcrop'])
                self.ui.models.item(2,column).setText('N/A')
                self.ui.models.item(3,column).setText(str(data['Byield']))
                self.ui.models.item(4,column).setText('N/a')
                self.ui.models.item(5,column).setText('N/a')
                self.ui.models.item(6,column).setText(str(data['BCHI']))
                self.ui.models.item(7,column).setText(data['BRR'])
                self.ui.models.item(8,column).setText(data['BFB'])
                self.ui.models.item(9,column).setText(data['Btil'])
            elif 'Pcrop' in data:
                self.ui.models.item(1,column).setText(data['Pcrop'])
                self.ui.models.item(2,column).setText(str(data['Ptree']))
                self.ui.models.item(3,column).setText(str(data['Pyield']))
                self.ui.models.item(4,column).setText(str(data['Pylo']))
                self.ui.models.item(5,column).setText(str(data['Pyhi']))
                self.ui.models.item(6,column).setText('N/a')
                self.ui.models.item(7,column).setText(data['PRR'])
                self.ui.models.item(8,column).setText(data['PFB'])
                self.ui.models.item(9,column).setText(data['Ptil'])




    def setLocation(self,locationID):
        # reset models
        self.resetModels()
        # set the new location
        self.locationID = locationID
        for v in self.locations.data[self.locationID].keys():
            if hasattr(self.ui,v):
                getattr(self.ui,v).setText(str(self.locations.data[self.locationID][v]))
        if 'img' in self.locations.data[self.locationID].keys():
            img = QtGui.QPixmap(self.locations.data[self.locationID]['img'])
            self.ui.imageBox.setPixmap(img.scaled(QtCore.QSize(self.ui.imageBox.size()),QtCore.Qt.KeepAspectRatio, QtCore.Qt.FastTransformation))
        self.datasets = Datasets()
        self.datasets.loadCfg(self.locations.data[self.locationID]['data'])

    def mapPick(self,coords):
        self.ui.longitudeEntry.setValue(coords[0])
        self.ui.latitudeEntry.setValue(coords[1])
        
        diff = numpy.zeros(len(self.locations.locations),float)
        for i in range(len(self.locations.locations)):
            d = numpy.array(self.locations.locations[i]) -numpy.array( coords)
            diff[i] = numpy.dot(d,d.transpose())

        self.setLocation(numpy.argmin(diff))

    @QtCore.pyqtSignature("double")
    def on_longitudeEntry_valueChanged(self,value):
        self.ui.mapWidget.plotPoint(value,  self.ui.latitudeEntry.value())

    @QtCore.pyqtSignature("double")
    def on_latitudeEntry_valueChanged(self,value):
        self.ui.mapWidget.plotPoint(self.ui.longitudeEntry.value(),value)
   
    @QtCore.pyqtSignature("")
    def on_addModelButton_clicked(self):
        self.addModel()
    
    @QtCore.pyqtSignature("")
    def on_saveFigureButton_clicked(self):
        file_name = QtGui.QFileDialog.getSaveFileName(self, "Save Figure", "", "PDF (*.pdf)")
        report = ShambaReport(file_name)
        #report.plot(self.ui.mapWidget.saveFigure,"Project Locations")

        report.plot(self.ui.plotWidget.saveFigure,"Green house gas emissions and removals [tCO2/ha/yr]")
        report.printDisclaimer()

        

if __name__ == "__main__":

    bmData = BasemapSetup()

    app = QtGui.QApplication(sys.argv)
    myapp = ShambaMain(basemapData=bmData)
    myapp.show()
    sys.exit(app.exec_())
