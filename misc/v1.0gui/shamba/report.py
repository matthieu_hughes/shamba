__all__ = ['ShambaReport']

from reportlab.pdfgen import canvas
from  reportlab.lib.pagesizes import A4
from reportlab.platypus import *
from reportlab.platypus.figures import ImageFigure
from reportlab.lib.units import cm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import utils

import tempfile,os,datetime

DISCLAIMER="""This tool has been prepared for general guidance on matters of interest only, and does not constitute professional advice. You should not act upon the information contained in this tool without obtaining specific professional advice. No representation or warranty (express or implied) is given as to the accuracy or completeness of the information contained in this tool, and, to the extent permitted by law, the authors, publishers and distributors of this tool do not accept or assume any liability, responsibility or duty of care for any consequences of you or anyone else acting, or refraining to act, in reliance on the information contained in this tool or for any decision based on it."""

class ShambaReport(object):
    def __init__(self,reportname):
        self.report = SimpleDocTemplate(reportname,pagesize=A4,
                                        rightMargin=72,leftMargin=72,
                                        topMargin=72,bottomMargin=18)
        self.fnames = []
        self.story = []

        self.width, self.height = A4

        now = datetime.datetime.now()
        self.styles = getSampleStyleSheet()
        #self.story.append(Paragraph("Shamba Report", self.styles["Title"]))
        #self.story.append(Paragraph(now.strftime("%d %B %Y"),self.styles["Heading1"]))
        
    def printDisclaimer(self):
        self.story.append(Paragraph(DISCLAIMER,self.styles["Italic"]))
        
    def plot(self,plotter,caption,width=16.5):
        f = tempfile.NamedTemporaryFile(suffix=".png",delete=False)
        fname = f.name
        self.fnames.append(fname)
        f.close()
        #plot image
        plotter(fname)
        # get aspect size
        img = utils.ImageReader(fname)
        iw, ih = img.getSize()
        aspect = ih / float(iw)
        # and add image
        im = Image(fname, width=width*cm, height=(width * aspect)*cm)
        self.story.append(im)


    def __del__(self):
        self.report.build(self.story)

        # delete temporary files
        for f in self.fnames:
            os.unlink(f)
