function [tSI_C, tSI_N, WB, tFire] = biomass3(BP, PP, growth, fire_occ, Mort, TH)

yrs = length(TH);
pools = {'leaf', 'branch', 'stem', 'croot', 'froot'};
np = length(pools);

[m, j] = fitcurves(growth.age, growth.dbh); 

% set initial conditions
TURN = zeros([1 np]); % TURN in y=0
SD(1) = BP.SD; % initial stand density
Dmean(1) = BP.Dmean; % initial DBH of trees
d_inc(1) = Dinc(m,j,Dmean(1)); % initial DBH increment
agb0 = ryan09(Dmean(1)); % initial AGB
T = PP.AL'.*agb0; % tree pool sizes in y=0

cf = zeros(yrs,1); cf = repmat(cf, [1 np]); % combustion factors
cf(fire_occ==1, 1:3) = 0.74; % fire only affects above-ground pools


% loop through yrs years and p pools

for i= 2:yrs
    for p = 1: length(pools)

        % MATT - CHECK THIS FOR TIMESTEP PROBLEM        
        TURN(i,p)= T(i-1,p).*PP.TO(p); % biomass turned over in year i
        Dmean(i,1) = ryan09inv(T(i-1,2)+T(i-1,3)); % works of stem and branch pool
        d_inc(i,1) = Dinc(m,j, Dmean(i)); % find DBH increment for year i
        NPP_AGB(i,1) = ryan09(Dmean(i)+d_inc(i)) - ryan09(Dmean(i)); % AGB increment in year i
        GROWTH(i,p) = NPP_AGB(i).*PP.AL(p); % AGB allocation to pools
        
        T(i,p) = T(i-1,p)+GROWTH(i,p)-TURN(i,p); % total pool sizes
        
        % soil inputs (tC/ha) - fire losses
        LI(i,p) = TURN(i,p).*SD(i-1).*(1-cf(i,p));
        DTI(i,p) = T(i,p).*PP.DTf(p).*Mort(i).*SD(i-1)*(1-cf(i,p));
        ThI(i,p) = T(i,p).*PP.THf(p).*TH(i).*SD(i-1)*(1-cf(i,p));
        
        % per pool N inputs (tN/ha) (- fire losses)
        N_LI(i,p) = LI(i,p)/PP.C(p)*PP.N(p);
        N_DT(i,p) = DTI(i,p)/PP.C(p)*PP.N(p);
        N_Th(i,p) = ThI(i,p)/PP.C(p)*PP.N(p);
        
        % per pool litter dry matter (tDM/ha)
        DM_LI(i,p) = (TURN(i,p)*SD(i-1))/PP.C(p);
        DM_DT(i,p) = (T(i,p).*PP.DTf(p).*Mort(i).*SD(i-1))/PP.C(p);
        DM_Th(i,p) = (T(i,p).*PP.THf(p).*TH(i).*SD(i-1))/PP.C(p);
        
                
    end
        
    SD(i,1) = SD(i-1)-(SD(i-1).*(Mort(i)+TH(i))); 
    WB(i,1) = (sum(T(i,:),2).*SD(i,1))./1000;
    tSI_C(i,1) = (sum(LI(i,:),2)+sum(ThI(i,:),2)+sum(DTI(i,:),2))./1000;% soil inputs tC/ha
    tSI_N(i,1)= (sum(N_LI(i,:),2)+sum(N_Th(i,:),2)+sum(N_DT(i,:),2))./1000; % soil inputs tN/ha
    tFire(i,1) = (sum(DM_LI(i,1:3),2)+sum(DM_Th(i,1:3),2)+sum(DM_DT(i,1:3),2))./1000; % litter for fire to consume (t AG DM/ha)
    
end

end
