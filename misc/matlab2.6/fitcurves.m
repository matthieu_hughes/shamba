function [m,p,rmse]=fitcurves(xobs,yobs)

% m is the best fit model type, and p are the fitted parameters for best fit model

% fit all the curves to data
[a,b,x1,y1]= optimumhypcurve(xobs,yobs); % hyperbolic
[c,x2,y2]= optimumlinear(xobs,yobs); % linear
[d,x3,y3]= optimumexpcurve(xobs,yobs); % exponential
[e,f,g,x4,y4]= optimumlogcurve(xobs,yobs); % logistic

% find the best fit
for i= 1:length(xobs)
    q(i,1)=(find(x1==xobs(i)));
    y(i,1)=y1(q(i));
    y(i,2)=y2(q(i));
    y(i,3)=y3(q(i));
    y(i,4)=y4(q(i));
end
for i=1:size(y,2)
    r(i)=sum((y(:,i)-yobs).^2); % calculate rmse for each
end

m=find(r==min(r)); % find best model
rmse = r(m);

if m==1
    p(1) = a;
    p(2) = b;
  else if m==2
        p(1) = c;
      else if m==3
              p(1) = d;
          else p(1) = e;
              p(2) = f;
              p(3) = g;
          end
      end
end

%figure('Color', [1 1 1])
%plot(xobs,yobs,'ok',x1,y1,'r',x2,y2,'b',x3,y3,'g',x4, y4,'c')
%title('Fitted growth curves')
%ylabel('DBH (cm)')
%xlabel('Age (years)')
%xlim([0 100])
%ylim([0 100])
%legend('Data',['Hyperbolic (',num2str(r(1)),')'],['Linear (', num2str(r(2)),')'],['Exponential (', num2str(r(3)),')'],... 
    %['Logistic (',num2str(r(4)),')'], 'Location', 'Best')

end
