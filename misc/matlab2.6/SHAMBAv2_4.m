%% Import data and set general parameters

clear all
close all

% location of project
lat = -18.591788;
long = 34.102968;

% project accounting period
d = 25; 

% growth of tree data
growth = SplitCSV('growth_measurements.csv'); % import project data 
xobs = growth.age; % age of trees measured
yobs = growth.dbh; % diameter of trees measured

% number of years for model to run, this depends on the growth model data
n_years = max(xobs)+20; % Need to decide if forecasting for 20 years is appropriate?

% crop parameters
crops = SplitCSV('crop.csv'); % with project data
b_crops = SplitCSV('crop_baseline.csv'); % baseline data

% soil cover
sc = [1 1 1 1 1 1 1 1 0 0 1 1]'; % soil cover for with project scenario
b_sc = [1 1 1 1 1 1 0 0 0 0 0 1]'; % soil cover of baseline scenario
u_sc = [1 1 1 1 1 1 1 1 1 1 1 1]'; % soil cover of undisturbed site (i.e. forest)

% fertiliser use
fert = SplitCSV('fertiliser.csv'); % with project data
b_fert = SplitCSV('fertiliser_base.csv'); % baseline data

% Set soil parameters 
% NOTE: If project starts in undisturbed state then values of SOC are the same !!
u_soc = 40; % soil organic C content (Mg C/ha) of undisturbed soils (miombo woodlands)
iom = 0.049*u_soc^1.139; % inert organic matter of undistrubed soils (is used throughout)
d_soc = 28.3; % soil organic C content (Mg C/ha) at year = 0 (from HWSD)
clay = 13; % soil clay content (%) (from HWSD or measurement at project site)

% agroforestry parameters (species specific)
SD(1) = 200; % initial Stand Density (trees/ha)
root_30 = 0.7; % proportion of roots in top 0-30 cm of soil
wood_c = 0.5; % wood C content
wood_d = 0.56; % wood density
wood_n = 0.028; % plant N content
wood_rs = 0.25; % root to shoot ratio

% biomass model parameters
TO = SplitCSV('Turnover.csv'); % turnover rates
THf = SplitCSV('Thinning.csv'); % inputs to soil at thinning

% thinning practices vector
TH = zeros(n_years,1); 
TH(10) = 0.5; % thin 0.5 at year 20
TH(20) = 0.2; % add additoinal thinning here

% Fire occurence with project
fri = 0; % expected fire return interval on field during project, 1 = burn every year
burn = 0; % are removed post-harvest crop residues burnt elsewhere off the farm every year? 1= yes, 0= no
fire_occ = zeros(n_years,1); fire_occ(fri:fri:n_years) = 1; % occurence of fire on field at intervals of fri
% Baseline fire
b_fri = 5; % expected fire return interval on field during baseline, 1 = burn every year
b_burn = 1; % are removed crop residues burnt elsewhere off the farm every year? 1= yes, 0 = no.
b_fire_occ = zeros(n_years,1); b_fire_occ(b_fri:b_fri:n_years) = 1; % occurence of fire at intervals of fri

% Additional forest litter input parameters
a_wood_c = 0.5; % litter C content (t C/t DM)
a_wood_n = 0.02; % litter N content (t N/t DM)

% Additional inputs to soils from forest litter
litter = 0.2; % t DM/ha of additional forest/woodland litter added to fields
litter_freq = 2; % Frequency of adding litter to fields (years)
add_litter = zeros(n_years,1); add_litter(litter_freq:litter_freq:n_years) = litter; % additional litter inputs t DM/ha
% baseline additional inputs
blitter = 0.2; % t DM/ha of additional forest/woodland litter added to fields
blitter_freq = 10; % Frequency of additional litter to fields (years)
badd_litter = zeros(n_years,1); badd_litter(blitter_freq:blitter_freq:n_years) = blitter; % additional litter inputs t DM/ha


%% Get climate data for project location
%  Data from CRU-TS 3.10.1 dataset.
% each month is the mean from 1960 to 1999 (I think!)

climate = zeros(12,3);

basename={'pre_','tmp_','pet_',};

for k=1:length(basename)
    for i=1:12
        filename=char(strcat(basename(k),num2str(i),'.txt'));
        data = importdata(filename,' ', 6);
        data = data.data;
        climate(i,k) = findclim(lat,long,data); % columns are rain, temp, evaporation
    end
end

% correct the data with the scaling factors
days = [31 30 31 30 31 30 31 31 30 31 30 31]';
climate = climate./10; % scaling factor of 10 used in CRU-TS dataset
climate(:,3) = climate(:,3).*(days); % pet is in mm/day, so convert to total mm/month

clear data filename basename k days

%% Run the crop models

% With project scenario 
[crop_soilinp, crop_n_inp, residues, off_farm] = crop(crops, fire_occ);

% Baseline scenario 
[bcrop_soilinp, bcrop_n_inp, bresidues, boff_farm] = crop(b_crops, b_fire_occ);

%% Run the Biomass model

% Model the growth curve with project data
m = fitcurves(xobs,yobs); 

% IF you want to change the model used manually, change the parameter m:
% m=1 is Hyperbolic, m=2 is linear, m=3 is exponential, and m=4 is
% logistic.

[tree_soilinp, tree_n_inp, total_biom, biom_growth, tree_fire] =...
    biomass(TH, TO, THf, SD, xobs, yobs, m, wood_rs, wood_c, wood_n, root_30, fire_occ);

% Here biom_growth is the increment of woody biomass in stems, branches,
% and coarse roots every time step. We need this for the final calculation
% of with project offsets. i.e. we need to annual growth of woody biomass,
% not the stock, to calculate the yearly offsets.

% note that the units are in t C/ha, t N/ha, t C/ha, tC/ha and t DM/ha

%% Calculate additional inputs to soils when adding forest litter

% with project
[add_soilinp, add_n_inp] = additional_input(add_litter, a_wood_c, a_wood_n, fire_occ);

% baseline
[badd_soilinp, badd_n_inp] = additional_input(badd_litter, a_wood_c, a_wood_n, b_fire_occ);

%% Run RothC 

% Calculate the rate modifyers for each scenario
u_rate = clim(climate(:,1),climate(:,2),climate(:,3),u_sc,clay); % undisturbed forests
b_rate = clim(climate(:,1),climate(:,2),climate(:,3),b_sc,clay); % baseline and initialisation rate
rate = clim(climate(:,1),climate(:,2),climate(:,3),sc,clay); % with project

% Run rothC to equlibrium under forest conditions 
u_input = 4.7382; % the tC/ha/year plant input of undisturbed sites. 
[e_soc, e_frac] = rothC_equilibrium(u_soc, clay, u_rate, u_input);

% To find u_input use the code below:
% u_input=(4:0.1:5)
%for i = 1:length(u_input)
 %   [e_soc(i)] = rothC_equilibrium(u_soc, clay, u_rate, u_input(i));
%end
%[f f] =min(abs(e_soc-u_soc));
%u_input = u_input(f);

% Initialise RothC to start year of project
[i_soc, i_frac]= rothC_SHAMBA(e_soc, e_frac, clay, iom, b_rate, bcrop_soilinp, 0, badd_soilinp); 
[f f] =min(abs(i_soc-d_soc));
i_soc = i_soc(f);% get values to start model with at year = 0
i_frac = i_frac(f,:);

% Run RothC for baseline 
bsoc = rothC_SHAMBA(i_soc, i_frac, clay, iom, b_rate, bcrop_soilinp, 0, badd_soilinp); 

% Run RothC for project scenario
soc = rothC_SHAMBA(i_soc, i_frac, clay, iom, rate, crop_soilinp, tree_soilinp, add_soilinp);

% All in units of t C/ha in the top 0-30 cm of soil

%% Calculate N2O emissions with fertiliser use

% baseline
[b_fert_n2o] = fertiliser(b_fert.msf,b_fert.nsf,b_fert.mof,b_fert.nof,b_fert.freq,n_years); % tCO2e/ha/year

% Project
[fert_n2o] = fertiliser(fert.msf,fert.nsf,fert.mof,fert.nof,fert.freq,n_years); % tCO2e/ha/year


%% Calculate N2O emissions with N inputs to soil
% calculates emissions from all N inputs from trees and crops

% baseline
[b_nfixer_n2o] = nfixer(bcrop_n_inp + badd_n_inp); % tCO2e/ha/year

% project
[nfixer_n2o] = nfixer(tree_n_inp + crop_n_inp + add_n_inp); % tCO2e/ha/year

%% Emissions from burning of crop residues, agroforestry litter and additional litter

% baseline
[b_fire_n2o] = fire(bresidues,boff_farm, 0, badd_litter, b_fire_occ, b_burn); % tCO2e/ha/year

% project
[fire_n2o] = fire(residues,off_farm, tree_fire, add_litter, fire_occ, burn); % tCO2e/ha/year

%% Calculate total project impact

% calculate soc changes at each time step
for i = 1:n_years
   delta_soc(i,1) = (soc(i+1)-soc(i))*(44/12); % tCO2e/ha, negatives are sinks here.
   delta_bsoc(i,1) = (bsoc(i+1)-bsoc(i))*(44/12);
end

% baseline emissions
B_RE = zeros(d,1);
for i = 1:d
    B_RE(i) = b_fire_n2o(i) + b_nfixer_n2o(i) + b_fert_n2o(i) -...
        delta_bsoc(i) - 0 ; % no agroforestry
end

% project emissions
P_RE = zeros(d,1);
for i = 1:d
    P_RE(i) = fire_n2o(i) + nfixer_n2o(i) + fert_n2o(i) - delta_soc(i)...
        - biom_growth(i)*(44/12); 
end

% total emissions saved with project over the accounting period d
ER = sum(P_RE)-sum(B_RE); %tCO2e/ha
ER_yr = P_RE-B_RE; 
%% Plots

% plot a graph of climate
figure('Color', [1 1 1])
bar(climate(:,1)), hold on
AX=plotyy((1:12),climate(:,3),(1:12),climate(:,2));
set(get(AX(1),'Ylabel'),'String','Rain and ET (mm/month)') 
set(get(AX(2),'Ylabel'),'String','Temperature (C)')
title('Climate'), xlabel('Month')
text(0,-20, ['Total rainfall =' , num2str(sum(climate(:,1))), ' mm'])

% emission comparisons
figure('Color',[1 1 1])
subplot(3,2,1)
plot([b_fire_n2o(1:d),fire_n2o(1:d)]), title('Fire emissions'), ylabel('tCO_2_e/ha')
subplot(3,2,2)
plot([b_nfixer_n2o(1:d),nfixer_n2o(1:d)]), title('N input emissions'), ylabel('tCO_2_e/ha')
subplot(3,2,3)
plot([b_fert_n2o(1:d),fert_n2o(1:d)]), title('Fertiliser emissions'), ylabel('tCO_2_e/ha')
legend('Without project', 'With project', 'Location', 'Best')
subplot(3,2,4)
plot([-1.*delta_bsoc(1:d), -1.*delta_soc(1:d)]), title('SOC changes'), ylabel('tCO_2_e/ha')
subplot(3,2,5)
plot(-1.*biom_growth(1:d).*(44/12), 'k'), title('Woody biomass growth'), ylabel('tCO_2_e/ha')
subplot(3,2,6)
plot([B_RE, P_RE]), title('Total emissions (tCO_2_e/ha)')
xlabel('Year'), ylabel('Total emissions (tCO_2_e/ha)')
text(1,-2, ['total = ',num2str(ER), ' tCO_2_e/ha'])
text(1,-4, ['total per year = ',num2str(ER/d), ' tCO_2_e/ha'])

% sink and sources 
pro=([-1.*delta_soc(1:d), fert_n2o(1:d), nfixer_n2o(1:d), fire_n2o(1:d), -1.*biom_growth(1:d).*(44/12)]);
sinks = pro.*(pro<0);
sources = pro.*(pro>0);
base=([-1.*delta_bsoc(1:d), b_fert_n2o(1:d), b_nfixer_n2o(1:d), b_fire_n2o(1:d)]);
bsinks = base.*(base<0);
bsources = base.*(base>0);

figure('Color', [1 1 1])
subplot(2,1,1)
H1=bar((1:d), bsinks, 0.5, 'stack');
hold on
H2=bar((1:d), bsources, 0.5, 'stack');
plot(B_RE, 'k', 'LineWidth',2)
title('Without project'),xlabel('Years'), ylabel('Emissions/removals (tCO_2_e/ha)')
set(H1(1), 'FaceColor', 'b');set(H1(2), 'FaceColor', 'c');set(H1(3), 'FaceColor', 'y');
set(H1(4), 'FaceColor', 'r');
set(H2(1), 'FaceColor', 'b');set(H2(2), 'FaceColor', 'c');set(H2(3), 'FaceColor', 'y');
set(H2(4), 'FaceColor', 'r');
clear H1 H2
subplot(2,1,2)
H1=bar((1:d), sinks, 0.5, 'stack');
hold on
H2=bar((1:d), sources, 0.5, 'stack');
plot(P_RE, 'k', 'LineWidth',2)
legend('SOC','Fertiliser','N inputs', 'Fire','Biomass') 
title('With project'),xlabel('Years'), ylabel('Emissions/removals (tCO_2_e/ha)')
set(H1(1), 'FaceColor', 'b');set(H1(2), 'FaceColor', 'c');set(H1(3), 'FaceColor', 'y');
set(H1(4), 'FaceColor', 'r');set(H1(5), 'FaceColor', 'g');
set(H2(1), 'FaceColor', 'b');set(H2(2), 'FaceColor', 'c');set(H2(3), 'FaceColor', 'y');
set(H2(4), 'FaceColor', 'r');set(H2(5), 'FaceColor', 'g');


