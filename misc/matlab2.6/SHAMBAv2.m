%% import data for the model

clear all
close all

% growth of tree data
growth = SplitCSV('growth_measurements.csv'); % import project data 
xobs = growth.age; % age of trees measured
yobs = growth.dbh; % diameter of trees measured

% climate data and soil cover (per month)
climate = SplitCSV('climate.csv');

% biomass parameters
AL = SplitCSV('Allocation.csv'); % allocation parameters
TO = SplitCSV('Turnover.csv'); % turnover rates
THf = SplitCSV('Thinning.csv'); % inputs to soil at thinning

% crop parameters
crops=SplitCSV('crop.csv'); % import data

% fertiliser use
fert = SplitCSV('fertiliser.csv');

% Set other parameters
n_years = 50; % number of years of rotation/project
clay = 13; % soil clay content (%)
soc = 28.3; % soil C content (Mg C/ha)

% agroforestry parameters (species specific)
SD(1) = 200; % initial Stand Density (trees/ha)
root_30 = 0.7; % proportion of roots in top 0-30 cm of soil
wood_c = 0.5; % wood C content
wood_d = 0.56; % wood density
wood_n = 0.028; % plant N content
agro = 1; % 1 if deciduous tropical woodland, 2 if shrubland

% thinning practices
TH = zeros(n_years,1); 
TH(20) = 0.5; % thin 0.5 at year 20
%TH(30) = 0.25; % add additoinal thinning here

% burning of post harvest residues
fri = 10; % expected fire return interval during project, 1 = burn every year


%% Run the climate model

% This gives you the rate modifyer for RothC and the env coefficient e for the chavez
% equation.

[rate,e] = clim(climate.rain,climate.temp,climate.et,climate.sc,clay);

% plot a graph of climate
figure('Color', [1 1 1])
bar(climate.rain), hold on
[AX,H1,H2]=plotyy((1:12),climate.et,(1:12),climate.temp);
set(get(AX(1),'Ylabel'),'String','Rain and ET (mm/month)') 
set(get(AX(2),'Ylabel'),'String','Temperature (C)')
title('Climate'), xlabel('Month')

%% Model the growth curve with project data

[m,rmse]=fitcurves(xobs,yobs); % find the best growth model to fit the data

% IF you want to change the model used manually, change the parameter m:
% m=1 is Hyperbolic, m=2 is linear, m=3 is exponential, and m=4 is
% logistic.

%% Run the Biomass model

clear pool thin turned soilinp offfarm meanD agb_inc D_inc 

% Set intial conditions
meanD(1) = 1;
agb_inc(1) = 0;
D_inc(1)=Dinc(xobs,yobs,m,0); % initial increment modelled

startpool.stem(1) = 0; % start of year pool
startpool.leaf(1) = 0; startpool.branch(1) = 0; startpool.froot(1) = 0; startpool.croot(1) = 0;

pool.stem(1) = 0; % total pool size
pool.leaf(1) = 0; pool.branch(1) = 0; pool.froot(1) = 0; pool.croot(1) = 0;

thin.stem(1) = 0;
thin.leaf(1) = 0; thin.branch(1) = 0; thin.froot(1) = 0; thin.lcroot(1) = 0;

soilinp.stem(1) = 0;
soilinp.leaf(1) = 0; soilinp.branch(1) = 0; soilinp.froot(1) = 0; soilinp.croot(1) = 0;

% Run the mass-balance model for each time step

for i = 2: n_years
           
    SD(i,1) = (1-TH(i,1)).*SD(i-1,1);
    
    D_inc(i,1) = Dinc(xobs,yobs,m,meanD(i-1)); % size dependant D_inc
    
    agb_inc(i,1) = SD(i,1).*(ryan09(D_inc(i)+meanD(i-1)) ...
                 - ryan09(meanD(i-1)));%.*SD(i); % calculate biomass increment 
      
    startpool.stem(i,1) = pool.stem(i-1) + agb_inc(i) * AL.stem; % growth first
    startpool.leaf(i,1) = pool.leaf(i-1) + agb_inc(i) * AL.leaf;
    startpool.branch(i,1) = pool.branch(i-1) + agb_inc(i) * AL.branch;
    startpool.froot(i,1) = pool.froot(i-1) + agb_inc(i) * AL.froot;
    startpool.croot(i,1) = pool.croot(i-1) + agb_inc(i) * AL.croot;
    
    % keep track of thinned mass (i.e. uses mass from the year before thinning)
    thin.stem(i,1) = pool.stem(i-1,1).*TH(i); 
    thin.leaf(i,1) = startpool.leaf(i-1,1).*TH(i); % this doesn't work if pool is zero
    thin.branch(i,1) = pool.branch(i-1,1).*TH(i); 
    thin.froot(i,1) = startpool.froot(i-1,1).*TH(i);
    thin.croot(i,1) = pool.croot(i-1,1).*TH(i);
    
    % keep track of turned over mass
    turned.stem(i,1) = startpool.stem(i,1).* TO.stem; % uses the same pool as the thin, so perhaps it's 
    turned.leaf(i,1) = startpool.leaf(i,1).* TO.leaf; % double counting here? 
    turned.branch(i,1) = startpool.branch(i,1).* TO.branch;
    turned.froot(i,1) = startpool.froot(i,1).* TO.froot;
    turned.croot(i,1) = startpool.croot(i,1).* TO.croot;
    
    % keep track of soil inputs
    soilinp.stem(i,1) = turned.stem(i,1) + thin.stem(i,1).*THf.stem;
    soilinp.leaf(i,1) = turned.leaf(i,1) + thin.leaf(i,1).*THf.leaf;
    soilinp.branch(i,1) = turned.branch(i,1) + thin.branch(i,1).*THf.branch;
    soilinp.froot(i,1) = (turned.froot(i,1) + thin.froot(i,1).*THf.froot)*root_30; % take account of roots in 0-30 cm
    soilinp.croot(i,1) = (turned.croot(i,1) + thin.croot(i,1).*THf.croot)*root_30;
    
    % keep track of off farm biomass (one pool)
    offfarm(i,1) = thin.stem(i,1).*(1-THf.stem)...
                  +thin.leaf(i,1).*(1-THf.leaf)...
                  +thin.branch(i,1).*(1-THf.branch)...
                  +thin.froot(i,1).*(1-THf.froot)...
                  +thin.croot(i,1).*(1-THf.croot);
    
    % biomass pools: stem
    pool.stem(i,1) = startpool.stem (i,1)... % add appropriate fraction of agb_inc
        - turned.stem(i,1)... % turnover from start pool
        - thin.stem(i,1); % remove mass to thinned pool
    
    %leaf
    pool.leaf(i,1) = startpool.leaf(i,1)...% add appropriate fraction of agb_inc
        - turned.leaf(i,1)... % turnover from start pool
        - thin.leaf(i,1); % remove mass to thinned pool
        
    %branch
    pool.branch(i,1) = startpool.branch(i,1)...% add appropriate fraction of agb_inc
        - turned.branch(i,1)- thin.branch(i,1);
    
    %fine roots
    pool.froot(i,1) = startpool.froot(i,1)...% add appropriate fraction of agb_inc
        - turned.froot(i,1) - thin.froot(i,1);
    
    %coarse roots
    pool.croot(i,1) = startpool.croot(i,1)...% add appropriate fraction of agb_inc
        - turned.croot(i,1) - thin.croot(i,1);
    
    meanD(i,1) = ryan09inv(pool.stem(i,1)./SD(i,1));% recalculate meanD now pools have incremented
    %./SD(i,1))
    
end

tree_soilinp = (soilinp.stem+soilinp.branch+soilinp.leaf+soilinp.froot+soilinp.croot)./1000;
tree_n_inp = tree_soilinp./wood_c.*wood_n;

total_biom = (pool.stem+pool.branch+pool.croot)./1000.*(44/12); % tCO2e/ha

% Plot it
figure('Color',[1 1 1])
subplot(2,2,1)
plot(meanD), title('meanD'), ylabel('DBH (cm)')
subplot(2,2,2)
plot(D_inc), title('D_i_n_c'), ylabel('DBH increment (cm)')
subplot(2,2,3)
plot([pool.stem pool.leaf pool.branch pool.froot pool.croot]./1000), title('pools'), ylabel('Mg C/ha')
legend('stem', 'leaf', 'branch','fine root', 'coarse root', 'Location', 'Best')
subplot(2,2,4)
plot([pool.stem+pool.branch pool.froot+pool.croot]./1000), title('AGB and BGG'),ylabel('Mg C/ha')
legend('AGB', 'BGB', 'Location', 'NorthWest')

figure('Color', [1 1 1])
subplot(2,1,1)
plot([soilinp.stem soilinp.branch soilinp.croot]./1000)
title('soil inputs'), ylabel('Mg C/ha')
legend('stem','branch','coarse root', 'Location', 'NorthWest')
subplot(2,1,2)
plot([soilinp.leaf soilinp.froot]./1000) 
title('soil inputs'), ylabel('Mg C/ha')
legend('leaf','fine roots', 'Location', 'NorthWest')

%% Run the crop model

[crop_soilinp, crop_n_inp, residues] = crop(crops.hyield_wt,crops.hi,...
    crops.resid,crops.crop_DMf,crops.crop_c,crops.crop_n,crops.crop_rs);

crop_soilinp = ones(n_years,1).*crop_soilinp;
crop_n_inp = ones(n_years,1).*crop_n_inp;
residues = ones(n_years,1).*residues;

%% Run RothC for with project scenario
clear input iom tstep iom inicon k p t C x options dpm rpm fraction total_soc

input=crop_soilinp + tree_soilinp;

tstep = 0:1; % 1 year time step

iom=0.049*soc^1.139; % inert organic matter 

inicon = [0.0601 7.4490 0.4952 17.9740 0]; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

[dpm, rpm] = dmprpm(crop_soilinp, tree_soilinp, agro); % calculate weighted DPM/RPM fractions based on soil input proportions

% set initial partitioning coefficients
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
%p(1) = dpm(1); % DPM fraction of input
p(2) = x/(x+1);
%p(3) = rpm(1); % RPM fraction

% set up output vectors
fractions=zeros(51,5);
total_soc=zeros(51,1);
% set initial conditions
total_soc(1)=sum(inicon(1:4))+iom;
fractions(1,:) = inicon;

% Run RothC for data inputs
options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:n_years
    
p(1)=dpm(i); % change dpm/rpm fractions
p(3)=rpm(i);

[t,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(i),rate),tstep,inicon,options);

inicon = C(end,:);

fractions(i+1,:)=C(end,:);

end

for i=2:51
total_soc(i) = sum(fractions(i,1:4))+iom; % total Soil C (Mg C/ha)
end


%% Calculate N2O emissions with fertiliser use

[fert_n2o] = fertiliser(fert.msf,fert.nsf,fert.mof,fert.nof,fert.freq,n_years); % tCO2e/ha/year

% emissions from fertilisers are spread out over the duration of the project

%% Calculate N2O emissions with N fixers
clear input

input = tree_n_inp+crop_n_inp; % assumes all crops and trees are N fixers here

[nfixer_n2o] = nfixer(input); % tCO2e/ha/year

%% Emissions from fire

[fire_n2o] = fire(residues,n_years,fri); % tCO2e/ha/year

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Baseline calculations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear crops msf nsf mof nof freq fri input climate fert

% climate data and soil cover (per month)
climate = SplitCSV('climate_baseline.csv'); % soil cover can change here

% crop parameters
crops=SplitCSV('crop_baseline.csv'); % import data

% fertiliser use
fert = SplitCSV('fertiliser_base.csv');

% burning of post harvest residues
fri = 10; % expected fire return interval during baseline, 1 = burn every year


%% climate for baseline

% This gives you the rate modifyer for RothC and the env coefficient e for the chavez
% equation.

[base_rate] = clim(climate.rain,climate.temp,climate.et,climate.sc,clay);

%% Run the crop model for baseline crops

[base_soilinp, base_n_inp, base_resid] = crop(crops.hyield_wt,crops.hi,...
    crops.resid,crops.crop_DMf,crops.crop_c,crops.crop_n,crops.crop_rs);

base_soilinp = ones(50,1).*base_soilinp;
base_n_inp = ones(50,1).*base_n_inp;
base_resid = ones(50,1).*base_resid;

%% Baseline N2O emissions with fertiliser use

[base_fert_n2o] = fertiliser(fert.msf,fert.nsf,fert.mof,fert.nof,fert.freq,n_years); % tCO2e/ha/year

% emissions from fertilisers are spread out over the duration of the project

%% baseline N2O emissions with N fixers

input = base_n_inp;

[base_nfixer_n2o] = nfixer(input); % tCO2e/ha/year

%% baseline Emissions from fire

[base_fire_n2o] = fire(base_resid,n_years,fri); % tCO2e/ha/year

%% Baseline RothC

clear input iom tstep iom inicon k p t C x options

input=base_soilinp;

tstep = 0:1; % 1 year time step

iom=0.049*soc^1.139; % inert organic matter 

inicon = [0.0601 7.4490 0.4952 17.9740 0]; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

% set initial partitioning coefficients
% Values based on RothC defaults for crop or improved grassland 
% (DPM/RPM =1.44)
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
p(1) = 0.56; % DPM fraction of input
p(2) = x/(x+1);
p(3) = 0.41; % RPM fraction

% set up output vectors
base_fractions=zeros(51,5);
base_soc=zeros(51,1);
% set initial conditions
base_soc(1)=sum(inicon(1:4))+iom;
base_fractions(1,:) = inicon;

% Run RothC for data inputs

options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:50
    
[t,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(i),base_rate),tstep,inicon,options);

inicon = C(end,:);

base_fractions(i+1,:)=C(end,:);

end

for i=2:51
base_soc(i) = sum(base_fractions(i,1:4))+iom;
end

% plot with project and without project
figure('Color', [1 1 1])
plot((0:50), total_soc, (0:50), base_soc), title('Soil C stocks')
xlabel('Years'), ylabel('Total C stocks (Mg C/ha)')
legend('With project', 'Without project')

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Total difference%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:n_years
    B_RE(i) = base_fire_n2o(i) + base_nfixer_n2o(i) + base_fert_n2o(i) -...
        (base_soc(i+1)-base_soc(i))*(44/12) - 0 ; % no agroforestry
    
    P_RE(i) = fire_n2o(i) + nfixer_n2o(i) + fert_n2o(i) - (total_soc(i+1)-total_soc(i))*(44/12)...
        - total_biom(i); % the change in tillage has not been added yet
end

% ??????? does the removals from AgroF need to be the growth rate of biomass,
% rather than total stocks? agb_inc is the flux, wich makes more sense.

ER = sum(B_RE)-sum(P_RE) % total emissions saved with project tCO2e/ha

figure('Color', [1 1 1])
plot((1:50),B_RE, (1:50),P_RE), title('Total emissions (Mg CO_2_e/ha)')
xlabel('Year'), ylabel('Total emissions (Mg CO_2_e/ha)')
legend ('Without project','With project', 'Location', 'SouthWest')

% the difference here is mostly from fire and fertiliser emissions. They
% make a big difference. The biomass as well. The soil C stocks are not
% hugely different. 


