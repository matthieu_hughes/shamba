%% Running Roth C to equilibrium

clear all

n=10000; % number of years to run model to equilibrium

soc = 40; % miombo woodlands have ~ 40 tC/ha in soils top 0-30 cm

clay = 13; % soil clay content (%)

sc = [1 1 1 1 1 1 1 1 1 1 1 1]';

climate = SplitCSV('climate_equilibrium_test.csv');

% calculate the rate modifyer for rothC 

rate = clim(climate.Rain,climate.Temp, climate.ET,sc,clay);

% Run RothC for with project scenario

plant_input = 4.6490; % the tC/ha/year plant inputs

input=ones(n,1)*plant_input; % make inputs a vector

tstep = 0:1; % 1 year time step

iom=0.049*soc^1.139; % inert organic matter 

inicon = [0 0 0 0 0]; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

% set initial partitioning coefficients
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
p(1) = 0.2; % DPM fraction of input
p(2) = x/(x+1);
p(3) = 0.46; % BIO fraction

% set up output vectors
fractions=zeros(n+1,5);
total_soc=zeros(n+1,1);
% set initial conditions
total_soc(1)=sum(inicon(1:4))+iom;
fractions(1,:) = inicon;

% Run RothC for data inputs
options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:n
    
[t,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(i),rate),tstep,inicon,options);

inicon = C(end,:);

fractions(i+1,:)=C(end,:);

end

for i=2:n+1
total_soc(i) = sum(fractions(i,1:4))+iom; % total Soil C (Mg C/ha)
end


total_soc(end)
fractions(end,1:4)
