function [a,xmod,ymod]=optimumlinear(xobs,yobs)

pars_i= [0.5];

pars_out=fminsearch(@(pars) costfunction(linear(xobs, pars), yobs),  pars_i);

a=pars_out(1);
xmod=([0:1:100]);
ymod=linear(xmod,pars_out);

end

function ymod=linear(xobs,pars)
a=pars(1);
ymod=a*xobs;
end

function rmse=costfunction(ymod,yobs)
rmse=sum((ymod-yobs).^2);
end
