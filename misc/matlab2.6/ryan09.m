function [S R T]= ryan09(dbh)

% allometric from my biotropica paper (2010) and thesis chapter 7, least squares power fit. dbh in cm, biomass in
% Mg C see "level1 all allometric data.xls" Updated as in paper carbon stocks overview

p_s =[2.60113411100682         -3.62875538418912];
p_r =[2.26189652008164         -3.37001300974884];
p_t =[2.54523999315796         -3.01808521403842];

S = polyval(p_s, log(dbh));S = exp(S);
R = polyval(p_r, log(dbh));R = exp(R);
T = polyval(p_t, log(dbh));T = exp(T);

%% to get the data to build this
% load('/Users/caseyrya/Documents/PhD/Outputs/thesis/all_destdata.mat')
% Bs = Ma
% Br = Mb
% D = DBH
% fnan = ~isnan(Br)
% Dr = D(fnan)
% % select only nhambitra plots
% 
% inv = delrowstruct(inv, find (inv.pid >5000));
% %inv = delrowstruct(inv, find (inv.dbh<10));
% 
% % create simple log-log allometric model
% f = find(~isnan(Mb));
% 
% p_s = polyfit(log(DBH), log(Ma),1);
% p_r = polyfit(log(DBH(f)), log(Mb(f)),1);
% p_t = polyfit(log(DBH(f)), log(sum([Ma(f) Mb(f)],2)),1);
% 
% S = polyval(p_s, log(dbh));S = exp(S);
% R = polyval(p_r, log(dbh));R = exp(R);
% T = polyval(p_t, log(dbh));T = exp(T);


%% here is the Data
%           DBH            AGB          BGB all in KgC
%            28          139         60.6
%          13.5         11.5          NaN
%          18.5         48.3         33.8
%            46        585.4        170.7
%          26.2          142         58.9
%          18.2           29         31.6
%          30.7        181.1         74.8
%            60          876        249.8
%          13.4          8.8          NaN
%            16         22.4          NaN
%            44          583        102.5
%            18         26.6         16.1
%          17.2         53.9         19.1
%            30        276.6         88.5
%            37        396.5        147.9
%          14.5         23.7            8
%          18.6         50.7            9
%          63.2       1021.9          572
%             6          2.3          1.8
%             5          1.9          1.3
%             6          2.8          4.6
%          22.5          111         63.8
%            42        677.1        192.3
%          32.3        451.9          156
%            72       3268.3        760.4
%          10.6           17          5.6
%            16         17.7          NaN
%            28        151.9          NaN
%             5          9.2          NaN
