function dC = ROTHC_year(t,C,k,p,input,rate)

% set partitioning coefficients
% (see Fig. 1, Scharnagl et al., 2010, Biogeosciences, 7:763-776)
xDPM = p(1);
xRPM = 1 - p(1);
xCO2 = p(2);
xBIOHUM = 1 - p(2);
xBIO = xBIOHUM*p(3);
xHUM = xBIOHUM*(1 - p(3));

% rate modifier is the combination of rate modifiers
% for temp, moist, and soil cover. 

% compute actual decomposition rates
kDPM = k(1)*rate;
kRPM = k(2)*rate;
kBIO = k(3)*rate;
kHUM = k(4)*rate;

% define system of ordinary differential equations
% (see Fig. 1, Scharnagl et al., 2010, Biogeosciences, 7:763-776)
dC = zeros(5,1);
dC(1) = input*xDPM - C(1)*kDPM;
dC(2) = input*xRPM - C(2)*kRPM;
dC(3) = (C(1)*kDPM + C(2)*kRPM + C(3)*kBIO + C(4)*kHUM)*xBIO - C(3)*kBIO;
dC(4) = (C(1)*kDPM + C(2)*kRPM + C(3)*kBIO + C(4)*kHUM)*xHUM - C(4)*kHUM;
dC(5) = (C(1)*kDPM + C(2)*kRPM + C(3)*kBIO + C(4)*kHUM)*xCO2;
