function [a,xmod,ymod]=optimumexpcurve(xobs,yobs)

pars_i= [0.5];

pars_out=fminsearch(@(pars) costfunction(expcurve(xobs, pars), yobs),  pars_i);

a=pars_out(1);
xmod=([0:1:100]);
ymod=expcurve(xmod,pars_out);
end

function ymod=expcurve(xobs,pars)
a=pars(1);
ymod=(1+a).^xobs;
end

function rmse=costfunction(ymod,yobs)
rmse=sum((ymod-yobs).^2);
end