function [add_soilinp, add_n_inp] = additional_input(add_litter, a_wood_c, a_wood_n, fire_occ)

% calculated the additional C and N soil inputs to the fields from addition
% of forest litter to the fields. 

    for i=1:length(fire_occ)
        if fire_occ(i,1) == 1
            add_soilinp(i,1) = add_litter(i)*a_wood_c*(1-0.74); % removes litter due to fire
            add_n_inp(i,1) = add_litter(i)*a_wood_n*(1-0.74); % removes litter due to fire
        else
            add_soilinp(i,1) = add_litter(i)*a_wood_c; % t C/ha
            add_n_inp(i,1) = add_litter(i)*a_wood_n; % t N/ha
        end
    end
    
end
        
