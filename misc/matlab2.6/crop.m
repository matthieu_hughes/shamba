function [crop_soilinp, crop_n_inp, residues, off_farm] = ...
    crop(crops, fire_occ)

% the structure 'crops' has the following data:
% hyield_wt = harvest yeidl wet weight (Mg /ha)
% hi = harvest index (fraction)
% resid = fraction of post-harvest residues left on field
% crop_DMf = crop dry matter fraction (Mg DM/Mg)
% crop_c = crop carbon content (fraction)
% crop_rs = crop root:shoot ratio

% fire_occ = vector of fire occurance

hyield_dw = crops.hyield_wt.*crops.crop_DMf; % harvest yeild dry weight (Mg DM/ha)

agb = hyield_dw./crops.hi; % AGB before harvest (Mg DM/ha)

resid_onfarm = agb.*crops.resid; % residues left after harvest (Mg DM/ha)

agb_input = sum(resid_onfarm.*crops.crop_c); % AGB inputs to soil at harvest (Mg C/ha) 

bgb_input = sum(agb.*crops.crop_rs.*crops.crop_c); % root inputs to soil at harvest (MgC/ha)

agb_n_inp = sum(resid_onfarm.*crops.crop_n); % (Mg N/ha)

bgb_n_inp = sum(agb.*crops.crop_rs.*crops.crop_n); % (Mg N/ha)

resid_offfarm = (1-(crops.hi+crops.resid)).*agb;

% calculate loss of crop soil inputs due to fire occurance
for i = 1:length(fire_occ)
    if fire_occ(i)==1
        fireloss(i,1)= agb_input*0.8; % assumes a combustion factor of 0.8 for crop residues (IPCC)
        n_loss(i,1) = agb_n_inp*0.8;
    else fireloss(i,1) = 0;
        n_loss(i,1) = 0;
    end
end

crop_soilinp = ones(length(fire_occ),1).*(agb_input + bgb_input) - fireloss; % Mg C/ha input to soils
crop_n_inp = ones(length(fire_occ),1).*(agb_n_inp + bgb_n_inp) - n_loss; % Mg N/ha input to soils
residues = ones(length(fire_occ),1)*sum(resid_onfarm); % Mg DM/ha residues on farm for fire to burn
off_farm = ones(length(fire_occ),1)*sum(resid_offfarm); % Mg DM/ha residues off farm for fire to burn

end
