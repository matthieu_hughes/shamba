function [fire_n2o] = fire(residues,off_farm, tree_fire, add_litter, fire_occ, burn)

% residues is crop residues available for fire to burn (Mg DM/ha) in field
% off_farm are residues removed from field (Mg DM/ha) every year
% tree_fire is the tree litter available for fire to burn (Mg DM/ha)
% add_litter are the additional inputs to fields from forest litter (Mg DM/ha)
% fire_occ is the occurence of fire during the length of the project
% burn is if removed residues are burnt every year? 1 = yes, 0 = no

% NB: this equation assumes a combustion factor of 0.8 (IPCC AFOLU table
% 2.6) for crop residues in post harvest field burning, 
% 0.74 for savanna woodlands and forest litter (mid/late dry season burn)

% emission factors are 2.7 for CH4, 0.07 for N2O in crop residues
% and 6.8 for CH4, 0.2 for N2O in tropical forest (i.e. woodland).
% table 2.5 IPCC

% global warming potentials are 21 for CH4, 310 for N2O
% all values from IPCC 2006 GHG Inventory

    crop_n2o = (residues.*0.8.*(2.7*21+0.07*310).*fire_occ); % residues on field burnt when a fire occurs 

    off_n2o = off_farm.*0.8.*(2.7*21+0.07*310); % residues taken off farm are burnt every year

    tree_n2o = (tree_fire.*0.74.*(6.8*21+0.2*310).*fire_occ); % agroforestry litter on field burnt when a fire occurs 
    
    add_n2o = (add_litter.*0.74.*(6.8*21+0.2*310).*fire_occ); % additional litter on field burnt when a fire occurs 

    if burn==1
        fire_n2o = (crop_n2o + tree_n2o + add_n2o + off_n2o).*10^-3; % tCO2e/ha
    else fire_n2o = (crop_n2o + tree_n2o + add_n2o).*10^-3; % tCO2e/ha
    end

end