
function [i_soc, i_frac]= rothC_initial(e_soc, e_frac, clay, d_soc, iom, b_rate, b_input)

% this function calculates the initial total SOC and fractions under
% baseline conditions at the start of the project to initialise the RothC Model
 
% NOTE: IT ONLY WORKS FOR CROPLANDS AND IMPROVED GRASSLAND

% inputs are:
% e_soc = total SOC of equilibrium soils
% e_frac = SOC fractions of equilibrium soils, 
% clay = soil clay content (%) of soils (from HWSD or for year = 0)
% d_soc = estimated/measured SOC of disturbed soils (t C/ha) at start of project
% iom = inert organic matter from the undisturbed soils
% b_rate = initial decay rate modifier 
% b_input = a vector of inputs to soil on a yearly basis (t C/ha/year) under baseline 

n=100; % number of years to run model with baseline scenario

input=sum(b_input)/length(b_input); % inputs are averaged as they vary between years due to fire

tstep = 0:1; % 1 year time step

inicon = e_frac; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

% set initial partitioning coefficients
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
p(1) = 0.59; % DPM fraction of input for CROPLAND!
p(2) = x/(x+1);
p(3) = 0.46; % BIO fraction

% set up output vectors
fractions=zeros(n+1,5);
total_soc=zeros(n+1,1);
% set initial conditions
total_soc(1) = e_soc;
fractions(1,:) = inicon;

% Run RothC for data inputs
options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:n
    
[~,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input,b_rate),tstep,inicon,options);

inicon = C(end,:);

fractions(i+1,:) = C(end,:);
end

for i=2:n+1
total_soc(i) = sum(fractions(i,1:4))+iom; % total Soil C (Mg C/ha)
end

% when does it reach SOC close to estimates at start of project (i.e. years after conversion?)
tmp = abs(total_soc-d_soc);
[f f] =min(tmp);

% get values to start model with at year = 0
i_soc = total_soc(f);
i_frac = fractions(f,:);

% NOTE: The values do not match up 100% with Roth C ouput, but we are only
% off by a few decimal points in most cases in terms of t C/ha in soil.

end