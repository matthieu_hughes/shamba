function dbh = ryan09inv(S)
% S is stem biomass kg
% dbh in cm

% ln(S)=(2.601*LN(DBH) -3.629)	eq.1

dbh = exp((log(S)+3.629)/2.601);