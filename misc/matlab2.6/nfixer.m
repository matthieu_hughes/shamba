function [nfixer_n2o] = nfixer(input)

% input is total N inputs from all sources of N fixers (crops and trees)
% (Mg N/ha/year)

% emission factor for N additions to soils is 0.01 kgN2O-N/kg N
% MW for N2O-N to N2O = 44/28
% GWP is 310 for N2O

nfixer_n2o =input.*0.01.*(44/28).*310;

end
