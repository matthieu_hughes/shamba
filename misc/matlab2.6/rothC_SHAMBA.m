function [soc, frac]= rothC_SHAMBA(i_soc, i_frac, clay, iom, rate, crop_soilinp,tree_soilinp, add_soilinp)

% this function calculates the changes to SOC and fractions for baseline
% and project activities, using the initial soc and fractions from spin up.

% inputs are:
% i_soc = total SOC at start of project
% i_frac = SOC fractions at start of project
% clay = soil clay content (%) of soil  (from HWSD or for year = 0)
% iom = inert organic matter from the initial forest cover
% rate = decay rate modifier apropriate for baseline or project scenario
% crop_soilinp = a vector of crop inputs to soil on a yearly basis (t C/ha/year) 
% tree_soilinp = a vector of agroforestry tree inputs to soil on a yearly basis (t C/ha/year) 
% add_soilinp = a vector of additional forest litter inputs to soils (t C/ha/year)


n = length(crop_soilinp);

input = crop_soilinp + tree_soilinp + add_soilinp;

tstep = 0:1; % 1 year time step

inicon = i_frac; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

% set initial partitioning coefficients
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
p(2) = x/(x+1);
p(3) = 0.46; % BIO fraction

dpm = dmprpm(crop_soilinp, tree_soilinp+add_soilinp); % calculate weighted DPM/RPM fractions based on soil input proportions
% NOTE: add_soilinp is assumed to be from forests/woodlands

% set up output vectors
fractions=zeros(n+1,5);
total_soc=zeros(n+1,1);
% set initial conditions
total_soc(1) = i_soc;
fractions(1,:) = inicon;

% Run RothC for data inputs
options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:n

    p(1)=dpm(i); % change dpm fractions
    
    [~,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(i),rate),tstep,inicon,options);

    inicon = C(end,:);

    fractions(i+1,:) = C(end,:);
end

for i=2:n+1
total_soc(i) = sum(fractions(i,1:4))+iom; % total Soil C (Mg C/ha)
end

soc = total_soc; % is the change in total SOC 
frac = fractions; % is the change in SOC fractions 

end