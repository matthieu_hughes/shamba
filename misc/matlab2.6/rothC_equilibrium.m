function [e_soc, e_frac]= rothC_equilibrium(u_soc, clay, u_rate, u_input)

% this function calculated the equilibrium SOC fractions under undisturbed
% conditions.

% NOTE: IT ONLY WORKS FOR WOODLANDS OR SAVANNA's

% inputs are:
% u_soc = SOC of undisturbed forested area, 
% clay = soil clay content (%) of soils (from HWSD or at year = 0)
% u_rate = decay rate modifier 
% input = the plant inputs to soil on a yearly basis (t C/ha/year)

n=1000; % number of years to run model to equilibrium

tstep = 0:1; % 1 year time step

iom=0.049*u_soc^1.139; % inert organic matter 

inicon = [0 0 0 0 0]; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

% set initial partitioning coefficients
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
p(1) = 0.2; % DPM fraction of input for WOODLANDS
p(2) = x/(x+1);
p(3) = 0.46; % BIO fraction

% set up output vectors
fractions=zeros(n+1,5);
total_soc=zeros(n+1,1);
% set initial conditions
total_soc(1) = 0;
fractions(1,:) = inicon;

% Run RothC for data inputs
options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:n
    
    [~,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,u_input,u_rate),tstep,inicon,options);

    inicon = C(end,:);

    fractions(i+1,:) = C(end,:);
end

for i=2:n+1
    total_soc(i) = sum(fractions(i,1:4))+iom; % total Soil organic C (Mg C/ha)
end

e_soc = total_soc(end); % is the total SOC at equilibrium
e_frac = fractions(end,:); % is the SOC fractions at equilibrium

end


