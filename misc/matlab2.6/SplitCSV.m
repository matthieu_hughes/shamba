function outCSV = SplitCSV(FN,DTformat,Tformat)
%--------------------------------------------------------------------------
% converts a CSV file to a structure
%
% a date/time string can (optionally) be given in the first field
%  or
% separate date and time fields can be given as the first two fields
%
% if the first field is a date/time string, its format is specifed 
% by the DTFormat string
%
% if the first two fields are date time strings, its format is specifed by
% the DTformat and Tformat strings
%
% if no date/time data is present no Format strings should be given
% 
% if a date/time field or date and time fields are present, a date time value 
% is calculated and is assigned the label 'DateTime'
%
% All other fields will be given the field name specified in the CSV file
% header line
%
%--------------------------------------------------------------------------
% create input list structure 
if (nargin == 1);
  ifrm = 1;
elseif (nargin == 2);
  if strcmp(upper(DTformat),'NONE')
    ifrm = 1;
  else
    ifrm = 2;
  end;
elseif (nargin == 3);
  ifrm = 3;
else;
    outCSV.DateTime = [];
    return;
end;
%----------------------------------------------------------------------
%fid=fopen('F:\robs matlab\SAS Griffin test5.csv');
fid=fopen(FN);
% read headers and sample line
hline = fgetl(fid);
sline = fgetl(fid);
fclose(fid);

% create cell array of headers and keep track of number of headers
rem = hline;
hsize = 0;
[vlabel, rem] = strtok(rem,',');
headers = cellstr(vlabel);
hsize = hsize + 1;
while not(isempty(strtrim(rem)))
    [vlabel, rem] = strtok(rem,',');
    headers = [headers cellstr(vlabel)];
    hsize = hsize + 1;
end;

% replace periods in headers with exclaimation points so that 
% structure creation will work
for ix = 1:hsize
    headers(ix) = strrep(headers(ix), '.','p');
    headers(ix) = strrep(headers(ix), '-','m');
end;

% read sample line as text cell array
scline = [',' sline ','];
spos = regexp(scline, ',');
sstart = spos;
send = spos;
[r1 c1] = size(spos);
sstart(c1) = [];
send(1) = [];
ssize = (send - sstart) -1;

sformat ='';
for ix = 1:(c1-1)
  if ssize(ix) == 0
       sformat = [sformat '%n '];
  else
   sval = sline(sstart(ix):(sstart(ix)+ssize(ix)-1));
   vval = str2double(sval);
   charsum = sum(not(isspace(sval)));
   if isnan(vval) 
       if (charsum > 0)
           sformat = [sformat '%s '];
       else
           sformat = [sformat '%n '];
       end;
   else
       sformat = [sformat '%n '];
   end
  end;
end;

% reset file
fid=fopen(FN);
M1 = textscan(fid, sformat, 'delimiter', ',', 'headerlines', 1, 'bufSize', 12288);
fclose(fid);

if ifrm == 2
  % save date time to structure - assumed to be in column 1
  outCSV.DateTime = datenum(M1{1},DTformat);
elseif ifrm == 3
  % save date time to structure - assumed to be in column 1
  [rm cm] = size(M1{1});
  blk = blanks(rm)';
  dstr = [char(M1{1}) blk  char(M1{2})];
  outCSV.DateTime = datenum(dstr ,[DTformat ' ' Tformat]) ;
%  outCSV.DateTime = outCSV.DateTime + datenum(M1{2},Tformat);
end;
[r1,c1] =size(headers);

% save other fields to structure 
for ix = ifrm:c1
    try
        s =  strcat('outCSV.',strtrim(headers(1,ix)),' = M1{ix};');
        s1 = char(s);
        eval(s1); 
    catch
        disp(['unable to create field: ', s1]);
    end;
end;
