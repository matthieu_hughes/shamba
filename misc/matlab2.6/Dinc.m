function [D_inc] = Dinc(m, p, meanD)

if m==1
    %[a,b]= optimumhypcurve(xobs,yobs); % hyperbolic parameters
    model1 = @(x) p(1)*p(2)*(exp(-p(2)*x)); % derivative of hyperbolic function
    D_inc=model1(meanD);
    else if m==2
            %[c]= optimumlinear(xobs,yobs); % slope
            D_inc=p(1); % slope = D_inc
        else if m==3
                %[d]= optimumexpcurve(xobs,yobs);
                model2 = @(x) ((p(1)+1).^x)*(log(p(1)+1)); % derivative of exponential
                D_inc = model2(meanD);
            else %[e,f,g]= optimumlogcurve(xobs,yobs); % logistic
                model3 = @ (x) (p(1)*p(2)*(exp(-p(2).*(x-p(3)))))./(exp(-p(2).*(x-p(3)))+1).^2;
                D_inc=model3(meanD);
            end
        end
end


end


