function b = chid97(dbh)
%% calculate biomass (kg, oven dry) from dbh in cm, from p93 Chidumayo97
small = dbh <10.1;
large = dbh >10.1;

        cord  = (2.23.*dbh -6.44) .*small;
        brush = (0.51.*dbh -0.57).*small;
        leaf  = (0.27.*dbh -0.47).*small;
     
        cordl  = (17.43.*dbh-188.84).*large;
        brushl =  (2.00.*dbh-9.40).*large;
        leafl  =  (0.59.*dbh-5.13).*large;
   
    cordt = sum([cord cordl],2);
    brusht = sum([brush brushl],2);
    leaft = sum([leaf leafl],2);
    
        
b = (cordt + brusht +leaft);

