function [a] = findclim(lat,long,data)

% a is the climate variables pre, pet or tmp from the CRU-TS 3.10.1 dataset for a specific
% lat long entered.  See CRU website for details of scaling
% factors for variables.

if lat<0
    x = ceil(180 + (lat*2*-1));
else x = ceil(180 - (lat*2));
end

y = ceil(360 + (long*2));

a = (data(x,y));

end