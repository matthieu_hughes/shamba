function [dpm, rpm] = dmprpm(crop_soilinp, tree_soilinp)

% calculates the weighted DPM/RPM fraction of soil inputs based on
% proportion of crop inputs (crop_frac) and tree inputs (tree_frac).

% DPM/RPM in roth C are: crops = 1.44 (dpm=59%, rpm=41%),
% and deciduous tropical woodland = 0.25 (dpm=20%,
% rpm=80%). If you have a mixed input, the DPM/RPM needs to be adjusted.

crop_frac = (crop_soilinp./(crop_soilinp+tree_soilinp)).*100; % percent of crop
tree_frac = 100-crop_frac; % percent of tree

dpm = ((59.*crop_frac./100)+(20.*tree_frac./100))/100; % weighted mean %
rpm = ((41.*crop_frac./100)+(80.*tree_frac./100))/100; 

end
