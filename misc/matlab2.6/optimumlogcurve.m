function [a,b,c,xmod,ymod]=optimumlogcurve(xobs,yobs)

pars_i= [50 0.3 11];

pars_out=fminsearch(@(pars) costfunction(logisticcurve(xobs, pars), yobs),  pars_i);

a=pars_out(1);
b=pars_out(2);
c=pars_out(3);
xmod=([0:1:100]);
ymod=logisticcurve(xmod,pars_out);

end

function ymod=logisticcurve(xobs,pars)
a=pars(1);
b=pars(2);
c=pars(3);
ymod=a./(1+(exp(-b.*(xobs-c))));
end

function rmse=costfunction(ymod,yobs)
rmse=sum((ymod-yobs).^2);
end
