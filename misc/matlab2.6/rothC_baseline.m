function [soc, frac]= rothC_SHAMBA(i_soc, i_frac, d_clay, iom, rate, plant_input, n_years)

% this function calculates the changes to SOC and fractions with and
% without project, using the initial soc and fractions from spin up.

% inputs are:
% i_soc = total SOC at start of project
% i_frac = SOC fractions at start of project
% d_clay = soil clay content (%) of disturbed soil at start
% iom = inert organic matter from the initial forest cover
% rate = decay rate modifier
% plant_input = a vector of plant inputs to soil on a yearly basis (t C/ha/year) 
% n_years = number of years to run model with baseline scenario

% Run RothC for with project scenario

n = n_years

input=b_input; % is a vector

tstep = 0:1; % 1 year time step

inicon = frac_0; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

% set initial partitioning coefficients
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay_0));
p(1) = 0.59; % DPM fraction of input for CROPLAND!
p(2) = x/(x+1);
p(3) = 0.46; % BIO fraction

% set up output vectors
fractions=zeros(n+1,5);
total_soc=zeros(n+1,1);
% set initial conditions
total_soc(1) = soc_0;
fractions(1,:) = inicon;

% Run RothC for data inputs
options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:n
    
[~,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(i),rate),tstep,inicon,options);

inicon = C(end,:);

fractions(i+1,:) = C(end,:);

end

for i=2:n+1
total_soc(i) = sum(fractions(i,1:4))+iom; % total Soil C (Mg C/ha)
end

soc = total_soc; % is the total SOC 
frac = fractions; % is the SOC fractions 

end