%% Testing the sensitivity of changing clay content in rothC

clay = (1:5:20);

for i=1:length(clay)

    % Calculate the rate modifyers for each scenario
    u_rate = clim(climate(:,1),climate(:,2),climate(:,3),u_sc,clay(i)); % undisturbed forests
    b_rate = clim(climate(:,1),climate(:,2),climate(:,3),b_sc,clay(i)); % baseline and initialisation rate
    rate = clim(climate(:,1),climate(:,2),climate(:,3),sc,clay(i)); % with project

    % Run rothC to equlibrium under forest conditions (THIS IS VERY SLOW)
    u_input = 5.03; % the tC/ha/year plant input of undistrubed sites. THIS NEEDS TO BE AUTOMATED!!!!!!
    [e_soc, e_frac] = rothC_equilibrium(u_soc, clay(i), u_rate, u_input);

    % Initialise RothC to start year of project
    [i_soc, i_frac]= rothC_initial(e_soc, e_frac, clay(i), d_soc, iom, b_rate, bcrop_soilinp);

    % Run RothC for baseline 
    [bsoc, bfrac]= rothC_SHAMBA(i_soc, i_frac, clay(i), iom, b_rate, bcrop_soilinp, 0); 

    % Run RothC for project scenario
    [soc, frac]= rothC_SHAMBA(i_soc, i_frac, clay(i), iom, rate, crop_soilinp, tree_soilinp);
       
    p_soc(:,i)=soc;
    b_soc(:,i)=bsoc;
end

figure('Color', [1 1 1])
plot([p_soc b_soc]), xlabel('Time step'), ylabel('SOC t C/ha')
legend('5','10','15','20','5','10','15','20')

figure('Color', [1 1 1])
plot((p_soc-b_soc)), xlabel('Time step'), ylabel('SOC difference')
legend('5','10','15','20')


for l=1:length(clay)
    for i=2:50
        delta_b(i,l)=(b_soc(i+1,l)-b_soc(i,l));
        delta_p(i,l)=(p_soc(i+1,l)-p_soc(i,l));
    end
end

figure('Color', [1 1 1])
plot([delta_p delta_b]), xlabel('Time step'), ylabel('SOC change')
legend('5','10','15','20','5','10','15','20')
figure('Color', [1 1 1])
plot(delta_p-delta_b), xlabel('Time step'), ylabel('SOC change difference')

sum(delta_p)
sum(delta_b)
sum(delta_p)-sum(delta_b)