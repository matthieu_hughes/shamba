%% baseline calculations

clear crops msf nsf mof nof freq fri input climate

% climate data and soil cover (per month)
climate = SplitCSV('climate_baseline.csv'); % soil cover can change here

% crop parameters
crops=SplitCSV('crop_baseline.csv'); % import data

% fertiliser use
msf = 0.5; % mass of synthetic fertiliser (Mg/ ha/ year)
nsf = 18; % nitrogen content of synthetic fertiliser (g N/ 100g fertiliser)
mof = 0; % mass of organic fertiliser (Mg/ ha/ year)
nof = 0; % N content of organic fertiliser (g N/ 100g fertiliser)
freq = 1; % application frequency (years)  (i.e. every year = 1, every 10 =10)

% burning of post harvest residues
fri = 1; % expected fire return interval during project, 1 = burn every year


%% Run the climate model

% This gives you the rate modifyer for RothC and the env coefficient e for the chavez
% equation.

[base_rate] = clim(climate.rain,climate.temp,climate.et,climate.sc,clay);

%% Run the crop model for baseline crops

[base_soilinp, base_n_inp, base_resid] = crop(crops.hyield_wt,crops.hi,...
    crops.resid,crops.crop_DMf,crops.crop_c,crops.crop_n,crops.crop_rs);

base_soilinp = ones(50,1).*base_soilinp;
base_n_inp = ones(50,1).*base_n_inp;
base_resid = ones(50,1).*base_resid;

%% Calculate N2O emissions with fertiliser use

[base_fert_n2o] = fertiliser(msf,nsf,mof,nof,freq,n_years); % tCO2e/ha/year

% emissions from fertilisers are spread out over the duration of the project

%% Calculate N2O emissions with N fixers

input = base_n_inp;

[base_nfixer_n2o] = nfixer(input); % tCO2e/ha/year

%% Emissions from fire

[base_fire_n2o] = fire(base_resid,n_years,fri); % tCO2e/ha/year

%% Baseline RothC

%% Run RothC 
clear input iom tstep iom inicon k p t C x options

input=base_soilinp;

tstep = 0:1; % 1 year time step

iom=0.049*soc^1.139; % inert organic matter 

inicon = [0.0601 7.4490 0.4952 17.9740 0]; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

% set initial partitioning coefficients
% Values based on RothC defaults for crop or improved grassland 
% (DPM/RPM =1.44)
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
p(1) = 0.56; % DPM fraction of input
p(2) = x/(x+1);
p(3) = 0.41; % RPM fraction

% set up output vectors
base_fractions=zeros(50,5);
base_soc=zeros(50,1);

% Run RothC for data inputs

options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

% run it for the first time step only
[t,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(1),rate),tstep,inicon,options);

% set initial conditions
base_soc(1)=sum(inicon(1:4))+iom;
base_fractions(1,:) = inicon;
base_fractions(2,:) = C(end,:);

% run it for the rest of the years
for i=2:50
    
inicon = C(end,:);

[t,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(i),rate),tstep,inicon,options);

base_fractions(i+1,:)=C(end,:);

end

for i=2:50
base_soc(i) = sum(base_fractions(i,1:4))+iom;
end

figure('Color', [1 1 1])
plot((1:50), total_soc, (1:50), base_soc), title('Soil C stocks')
xlabel('Years'), ylabel('Total C stocks (Mg C/ha)')
legend('With project', 'Without project')

