%% import data for the model

clear all
close all

% location of project
lat = -18.591788;
long = 34.102968;

% growth of tree data
growth = SplitCSV('growth_measurements.csv'); % import project data 
xobs = growth.age; % age of trees measured
yobs = growth.dbh; % diameter of trees measured

% biomass parameters
% AL = SplitCSV('Allocation.csv'); % allocation parameters
TO = SplitCSV('Turnover.csv'); % turnover rates
THf = SplitCSV('Thinning.csv'); % inputs to soil at thinning

% crop parameters
crops=SplitCSV('crop.csv'); % import data

% soil cover
sc = [1 1 1 1 1 1 1 1 0 0 1 1]';

% fertiliser use
fert = SplitCSV('fertiliser.csv');

% Set other parameters
n_years = 50; % number of years of rotation/project
clay = 13; % soil clay content (%)
soc = 28.3; % soil C content (Mg C/ha)

% agroforestry parameters (species specific)
SD(1) = 200; % initial Stand Density (trees/ha)
root_30 = 0.7; % proportion of roots in top 0-30 cm of soil
wood_c = 0.5; % wood C content
wood_d = 0.56; % wood density
wood_n = 0.028; % plant N content
wood_rs = 0.3; % root to shoot ratio

% thinning practices vector
TH = zeros(n_years,1); 
TH(20) = 0.5; % thin 0.5 at year 20
%TH(30) = 0.25; % add additoinal thinning here

% burning of post harvest residues
fri = 0; % expected fire return interval during project, 1 = burn every year
off_burn = 0; % are removed post-harvest crop residues burnt elsewhere off the farm? 1= yes, 0= no
fire_occ = zeros(n_years,1); 
fire_occ(fri:fri:n_years) = 1; % occurence of fire at intervals of fri

%% Run the climate model

% This gives you the rate modifyer for RothC and the env coefficient e for the chavez
% equation. Data from CRU-TS 3.10.1 dataset.

climate = zeros(12,3);

basename={'pre_','tmp_','pet_',};

for k=1:length(basename)
    for i=1:12
        filename=char(strcat(basename(k),num2str(i),'.txt'));
        data = importdata(filename,' ', 6);
        data = data.data;
        climate(i,k) = findclim(lat,long,data); % columns are pet, pre, tmp
    end
end

% correct the data with the scaling factors
days = [31 30 31 30 31 30 31 31 30 31 30 31]';
climate = climate./10; % scaling factor of 10 used in CRU-TS dataset
climate(:,3) = climate(:,3).*(days); % pet is in mm/day, so convert to total mm/month

% calculate the rate modifyer for rothC and e variable for chavez equation
% in biomass model
[rate,e] = clim(climate(:,1),climate(:,2),climate(:,3),sc,clay);

% plot a graph of climate
figure('Color', [1 1 1])
bar(climate(:,1)), hold on
[AX,H1,H2]=plotyy((1:12),climate(:,3),(1:12),climate(:,2));
set(get(AX(1),'Ylabel'),'String','Rain and ET (mm/month)') 
set(get(AX(2),'Ylabel'),'String','Temperature (C)')
title('Climate'), xlabel('Month')
text(0,-20, ['Total rainfall =' , num2str(sum(climate(:,1))), ' mm'])

clear data filename basename k i AX H1 H2

%% Model the growth curve with project data

[m,rmse]=fitcurves(xobs,yobs); % find the best growth model to fit the data

% IF you want to change the model used manually, change the parameter m:
% m=1 is Hyperbolic, m=2 is linear, m=3 is exponential, and m=4 is
% logistic.

%% Run the Biomass model

clear pool thin turned soilinp offfarm meanD agb_inc D_inc 

% Set intial conditions
meanD(1) = 1;
agb_inc(1) = 0;
D_inc(1)=Dinc(xobs,yobs,m,0); % initial increment modelled

startpool.stem(1) = 0; % start of year pool
startpool.leaf(1) = 0; startpool.branch(1) = 0; startpool.froot(1) = 0; startpool.croot(1) = 0;

pool.stem(1) = 0; % total pool size
pool.leaf(1) = 0; pool.branch(1) = 0; pool.froot(1) = 0; pool.croot(1) = 0;

thin.stem(1) = 0;
thin.leaf(1) = 0; thin.branch(1) = 0; thin.froot(1) = 0; thin.lcroot(1) = 0;

soilinp.stem(1) = 0;
soilinp.leaf(1) = 0; soilinp.branch(1) = 0; soilinp.froot(1) = 0; soilinp.croot(1) = 0;

% create models for estimating the allocation to leaf and branches from dbh
% These models are based on data from chidumayo on allometrics of miombo
% trees
al_branch = @(x) 8.1714.*x.^-1.258;
al_leaf = @(x) 1.59.*x.^-1.285;


% Run the mass-balance model for each time step

for i = 2: n_years
           
    SD(i,1) = (1-TH(i,1)).*SD(i-1,1);
    
    D_inc(i,1) = Dinc(xobs,yobs,m,meanD(i-1)); % size dependant D_inc
    
    meanD(i,1) = meanD(i-1)+D_inc(i);
    
    agb_inc(i,1) = SD(i,1).*(ryan09(meanD(i))...
                 - ryan09(meanD(i-1))); % calculate biomass increment of stem
    
    % Growth of pools based on allometric to stem biomass
    startpool.stem(i,1) = pool.stem(i-1) + agb_inc(i); % growth of stem first (or cordwood)
    startpool.leaf(i,1) = startpool.stem(i) * al_leaf(meanD(i)); % dynamic allocation
    startpool.branch(i,1) = startpool.stem(i) * al_branch(meanD(i));
    startpool.froot(i,1) = startpool.stem(i) * al_leaf(meanD(i));
    startpool.croot(i,1) = startpool.stem(i) * wood_rs; % non dynamic allocation
    
    % keep track of thinned mass (uses mass from the year before thinning)
    thin.stem(i,1) = pool.stem(i-1,1).*TH(i); 
    % thin.leaf(i,1) = startpool.leaf(i-1,1).*TH(i); % this doesn't work if pool is zero
    thin.branch(i,1) = pool.branch(i-1,1).*TH(i); 
    % thin.froot(i,1) = startpool.froot(i-1,1).*TH(i);
    thin.croot(i,1) = pool.croot(i-1,1).*TH(i);
    
    % keep track of turned over mass
    turned.stem(i,1) = startpool.stem(i,1).* TO.stem;
    turned.leaf(i,1) = startpool.leaf(i,1).* TO.leaf; 
    turned.branch(i,1) = startpool.branch(i,1).* TO.branch;
    turned.froot(i,1) = startpool.froot(i,1).* TO.froot;
    turned.croot(i,1) = startpool.croot(i,1).* TO.croot;
    
    
    % keep track of the soil inputs (if fire, agb inputs are decreased by 74%)
    % based on IPCC combustion factor for savanna woodland (table 2.6)
    if fire_occ(i,1) == 1
        soilinp.stem(i,1) = (turned.stem(i,1) + thin.stem(i,1).*THf.stem).*(1-0.74);
        soilinp.leaf(i,1) = (turned.leaf(i,1)).*(1-0.74);% + thin.leaf(i,1).*THf.leaf;
        soilinp.branch(i,1) = (turned.branch(i,1) + thin.branch(i,1).*THf.branch).*(1-0.74);
      else   soilinp.stem(i,1) = turned.stem(i,1) + thin.stem(i,1).*THf.stem;
             soilinp.leaf(i,1) = turned.leaf(i,1);% + thin.leaf(i,1).*THf.leaf;
             soilinp.branch(i,1) = turned.branch(i,1) + thin.branch(i,1).*THf.branch;
    end
        
        soilinp.froot(i,1) = turned.froot(i,1)*root_30; % + thin.froot(i,1).*THf.froot)
        soilinp.croot(i,1) = (turned.croot(i,1) + thin.croot(i,1).*THf.croot)*root_30; % take account of roots in 0-30 cm
    
    % keep track of AGB litter available for fire to consume
    tree_fire(i,1) = ((turned.stem(i,1) + thin.stem(i,1).*THf.stem)...
        + (turned.branch(i,1) + thin.branch(i,1).*THf.branch)...
        + turned.leaf(i,1))./1000./wood_c; % Mg DM/ha
        
    % keep track of off farm biomass (one pool)
    offfarm(i,1) = thin.stem(i,1).*(1-THf.stem)...
                  +thin.branch(i,1).*(1-THf.branch)...
                  +thin.croot(i,1).*(1-THf.croot);
                  %+thin.froot(i,1).*(1-THf.froot)...
                  %+thin.leaf(i,1).*(1-THf.leaf)...

    % biomass pools: stem
    pool.stem(i,1) = startpool.stem (i,1)... % add appropriate fraction of agb_inc
        - turned.stem(i,1)... % turnover from start pool
        - thin.stem(i,1); % remove mass to thinned pool
    
    %leaf
    pool.leaf(i,1) = startpool.leaf(i,1)...% add appropriate fraction of agb_inc
        - turned.leaf(i,1); %... % turnover from start pool
        % - thin.leaf(i,1); % remove mass to thinned pool
        
    %branch
    pool.branch(i,1) = startpool.branch(i,1)...% add appropriate fraction of agb_inc
        - turned.branch(i,1)- thin.branch(i,1);
    
    %fine roots
    pool.froot(i,1) = startpool.froot(i,1)...% add appropriate fraction of agb_inc
        - turned.froot(i,1); % - thin.froot(i,1);
    
    %coarse roots
    pool.croot(i,1) = startpool.croot(i,1)...% add appropriate fraction of agb_inc
        - turned.croot(i,1) - thin.croot(i,1);
    
    meanD(i,1) = ryan09inv(pool.stem(i,1)./SD(i,1));% recalculate meanD now pools have incremented
  
end

tree_soilinp = (soilinp.stem+soilinp.branch+soilinp.leaf+soilinp.froot+soilinp.croot)./1000; % Mg C /ha
tree_n_inp = tree_soilinp./wood_c.*wood_n; % Mg N/ha
basal_area = ((pi().*(meanD./2).^2)./10000).*SD; % m2/ha
LAI = (((startpool.leaf./0.5)./10000).*1000)./79; % m2 leaf area/m2 ground area. Assumes LMA = 79 g/m2

total_biom = (pool.stem+pool.branch+pool.croot)./1000.*(44/12); % total woody biomass tCO2e/ha

% Plot it
figure('Color',[1 1 1])
subplot(2,2,1)
plot(meanD), title('meanD'), ylabel('DBH (cm)')
subplot(2,2,2)
plot(D_inc), title('D_i_n_c'), ylabel('DBH increment (cm)')
subplot(2,2,3)
plot([pool.stem pool.leaf pool.branch pool.froot pool.croot]./1000), title('pools'), ylabel('Mg C/ha')
legend('stem', 'leaf', 'branch','fine root', 'coarse root', 'Location', 'Best')
subplot(2,2,4)
plot([pool.stem+pool.branch pool.froot+pool.croot]./1000), title('AGB and BGG'),ylabel('Mg C/ha')
legend('AGB', 'BGB', 'Location', 'NorthWest')

figure('Color', [1 1 1])
subplot(2,1,1)
plot([soilinp.stem soilinp.branch soilinp.croot]./1000)
title('soil inputs'), ylabel('Mg C/ha')
legend('stem','branch','coarse root', 'Location', 'NorthWest')
subplot(2,1,2)
plot([soilinp.leaf soilinp.froot]./1000) 
title('soil inputs'), ylabel('Mg C/ha')
legend('leaf','fine roots', 'Location', 'NorthWest')

figure('Color', [1 1 1])
subplot(2,1,1)
plot(basal_area)
title('Basal area'), ylabel('m^2/ha')
subplot(2,1,2)
plot(LAI) 
title('Leaf area index'), ylabel('LAI (m^2 m^-^2')

%% Run the crop model

[crop_agbinp, crop_bgbinp, crop_ninp,resid] = ...
    crop(crops.hyield_wt,crops.hi,crops.resid,crops.crop_DMf,...
    crops.crop_c, crops.crop_n, crops.crop_rs, off_burn);

% calculate loss of crop soil inputs due to fire occurance
for i = 1:n_years
    if fire_occ(i)==1
        crop_fireloss(i,1)= crop_agbinp*0.8; % assumes a combustion factor of 0.8 for crop residues (IPCC)
        crop_n_inp(i,1) = crop_ninp*0.8;
    else crop_fireloss(i,1) = 0;
        crop_n_inp(i,1) = crop_ninp;
    end
end

crop_fire = ones(50,1).*resid; % total available residues for fire to burn (Mg DM/ha)
crop_soilinp = ones(50,1).*(crop_agbinp+crop_bgbinp)- crop_fireloss; % Mg C/ha


%% Run RothC for with project scenario
clear input iom tstep iom inicon k p t C x options dpm rpm fraction total_soc

input=crop_soilinp + tree_soilinp;

tstep = 0:1; % 1 year time step

iom=0.049*soc^1.139; % inert organic matter 

inicon = [0.0601 7.4490 0.4952 17.9740 0]; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

[dpm, rpm] = dmprpm(crop_soilinp, tree_soilinp); % calculate weighted DPM/RPM fractions based on soil input proportions

% set initial partitioning coefficients
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
%p(1) = dpm(1); % DPM fraction of input
p(2) = x/(x+1);
%p(3) = rpm(1); % RPM fraction

% set up output vectors
fractions=zeros(51,5);
total_soc=zeros(51,1);
% set initial conditions
total_soc(1)=sum(inicon(1:4))+iom;
fractions(1,:) = inicon;

% Run RothC for data inputs
options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:n_years
    
p(1)=dpm(i); % change dpm/rpm fractions
p(3)=rpm(i);

[t,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(i),rate),tstep,inicon,options);

inicon = C(end,:);

fractions(i+1,:)=C(end,:);

end

for i=2:51
total_soc(i) = sum(fractions(i,1:4))+iom; % total Soil C (Mg C/ha)
end


%% Calculate N2O emissions with fertiliser use

[fert_n2o] = fertiliser(fert.msf,fert.nsf,fert.mof,fert.nof,fert.freq,n_years); % tCO2e/ha/year


%% Calculate N2O emissions with N fixers

% assumes all trees and crops are N fixers

[nfixer_n2o] = nfixer(tree_n_inp+crop_n_inp); % tCO2e/ha/year

%% Emissions from fire from crop and tree litter

[fire_n2o] = fire(crop_fire, tree_fire ,fire_occ); % tCO2e/ha/year

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Baseline calculations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear crops msf nsf mof nof freq fri sc fert fire_occ rate off_burn

% climate data and soil cover (per month)
sc = [1 1 1 1 1 1 0 0 0 0 0 1]';

% crop parameters
crops=SplitCSV('crop_baseline.csv'); % import data

% fertiliser use
fert = SplitCSV('fertiliser_base.csv');

% burning of post harvest residues
fri = 5; % expected fire return interval during baseline, 1 = burn every year
off_burn = 1; % are removed crop residues burnt elsewhere off the farm? 1= yes, 0 = no.
fire_occ = zeros(n_years,1); 
fire_occ(fri:fri:n_years) = 1; % occurence of fire at intervals of fri


%% climate for baseline

% This gives you the rate modifyer for RothC and the env coefficient e for the chavez
% equation.

[rate] = clim(climate(:,1),climate(:,2),climate(:,3),sc,clay);

%% Run the crop model for baseline crops
clear crop_agbinp crop_bgbinp resid crop_fireloss crop_ninp 

[crop_agbinp, crop_bgbinp, crop_ninp,resid] = ...
    crop(crops.hyield_wt,crops.hi,crops.resid,crops.crop_DMf,...
    crops.crop_c,crops.crop_n,crops.crop_rs, off_burn);

% calculate loss of crop soil inputs due to fire occurance
for i = 1:n_years
    if fire_occ(i)==1
        crop_fireloss(i,1)= crop_agbinp*0.8; % assumes a combustion factor of 0.8 for crop residues (IPCC)
        base_crop_n_inp(i,1) = crop_ninp*0.8;
    else crop_fireloss(i,1) = 0;
        base_crop_n_inp(i,1) = crop_ninp;
    end
end

base_crop_fire = ones(50,1).*resid; % total available residues for fire to burn (Mg DM/ha)
base_soilinp = ones(50,1).*(crop_agbinp+crop_bgbinp)-crop_fireloss; % Mg C/ha


%% Baseline N2O emissions with fertiliser use

[base_fert_n2o] = fertiliser(fert.msf,fert.nsf,fert.mof,fert.nof,fert.freq,n_years); % tCO2e/ha/year

% emissions from fertilisers are spread out over the duration of the project

%% baseline N2O emissions with N fixers

% no N emissions as no agroforestry N fixers used in baseline

[base_nfixer_n2o] = nfixer(base_crop_n_inp); % tCO2e/ha/year

%% baseline Emissions from fire of crop residue burning

[base_fire_n2o] = fire(base_crop_fire, 0, fire_occ); % tCO2e/ha/year

%% Baseline RothC

clear input iom tstep iom inicon k p t C x options

input=base_soilinp;

tstep = 0:1; % 1 year time step

iom=0.049*soc^1.139; % inert organic matter 

inicon = [0.0601 7.4490 0.4952 17.9740 0]; % set inital condition of SOC pool [c_DPM c_RPM c_BIO c_HUM CO2] (t C ha-1)

k = [10 0.3 0.66 0.02]; % set decomposition rate constants [k_DPM k_RPM k_BIO k_HUM] (year^-1)

% set initial partitioning coefficients
% Values based on RothC defaults for crop or improved grassland 
% (DPM/RPM =1.44)
x  = 1.67*(1.85 + 1.6*exp(-0.0786*clay));
p(1) = 0.56; % DPM fraction of input
p(2) = x/(x+1);
p(3) = 0.41; % RPM fraction

% set up output vectors
base_fractions=zeros(51,5);
base_soc=zeros(51,1);
% set initial conditions
base_soc(1)=sum(inicon(1:4))+iom;
base_fractions(1,:) = inicon;

% Run RothC for data inputs

options = odeset('RelTol',1e-3); % set some options of numerical solution scheme

for i=1:50
    
[t,C] = ode45(@(t,C) ROTHC_year(t,C,k,p,input(i),rate),tstep,inicon,options);

inicon = C(end,:);

base_fractions(i+1,:)=C(end,:);

end

for i=2:51
base_soc(i) = sum(base_fractions(i,1:4))+iom;
end

% plot with project and without project
figure('Color', [1 1 1])
plot((0:50), total_soc, (0:50), base_soc), title('Soil C stocks')
xlabel('Years'), ylabel('Total C stocks (Mg C/ha)')
legend('With project', 'Without project')

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Total difference %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear B_RE P_RE total_growth

for i = 1:n_years
    B_RE(i) = base_fire_n2o(i) + base_nfixer_n2o(i) + base_fert_n2o(i) -...
        (base_soc(i+1)-base_soc(i))*(44/12) - 0 ; % no agroforestry
end

for i=2:50
total_growth (i,1) = (total_biom(i)-total_biom(i-1)).*(44/12); % tCO2/ha
if total_growth(i,1) <0
    total_growth(i,1) = NaN;
end
end

for i = 1:50
    P_RE(i) = fire_n2o(i) + nfixer_n2o(i) + fert_n2o(i) - ((total_soc(i+1)-total_soc(i))*(44/12))...
        - total_growth(i); % the change in tillage has not been added yet
end

figure('Color',[1 1 1])
subplot(3,2,1)
plot((1:50),base_fire_n2o,(1:50),fire_n2o), title('Fire emissions'), ylabel('tCO_2_e/ha')
legend('Without project', 'With project')
subplot(3,2,2)
plot((1:50),base_nfixer_n2o,(1:50),nfixer_n2o), title('N fixer emissions'), ylabel('tCO_2_e/ha')
legend('Without project', 'With project')
subplot(3,2,3)
plot((1:50),base_fert_n2o,(1:50),fert_n2o), title('Fertiliser emissions'), ylabel('tCO_2_e/ha')
legend('Without project', 'With project')
subplot(3,2,4)
plot((1:51),base_soc.*(44/12),(1:51),total_soc.*(44/12)), title('Soil C'), ylabel('tCO_2_e/ha')
legend('Without project', 'With project')
subplot(3,2,5)
plot((1:50),total_growth), title('Total Woody Biomass Growth'), ylabel('tCO_2_e/ha')

% total emissions saved with project tCO2e/ha
ER = nansum(P_RE)-nansum(B_RE) 

figure('Color', [1 1 1])
plot((1:50),B_RE, (1:50),P_RE), title('Total emissions (Mg CO_2_e/ha)')
xlabel('Year'), ylabel('Total emissions (Mg CO_2_e/ha)')
legend ('Without project','With project', 'Location', 'Best')


