function [fert_emiss] = fertiliser(fert,n_years)

% equation from CDM AR methodology, emission factors from IPCC (2006)

msf = fert.msf; % mass of synthetic fertiliser (Mg/ ha/ year)
nsf = fert.nsf; %nitrogen content of synthetic fertiliser (g N/ 100g fertiliser)
mof = fert.mof; % mass of organic fertiliser (Mg/ ha/ year)
nof = fert.nof; % N content of organic fertiliser (g N/ 100g fertiliser)
freq = fert.freq; % application frequency (years)  (i.e. every year = 1, every 10 =10)
% n_years = duration of project

% fert_emiss = tCO2e/ha/year 

fert_occ = zeros(n_years,1);
fert_occ(freq:freq:n_years) = 1;

fert_emiss = (((msf*nsf*(1-0.1))+(mof*nof*(1-0.2)))*0.01*(44/28)*310).*fert_occ;

end
