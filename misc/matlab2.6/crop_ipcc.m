function [crop_CI, crop_NI, on_farm, off_farm] = ...
    crop_ipcc(crops,crop30,fire_occ)

% This calculates crop residues and soil inputs as defined by IPCC (2006) National GHG
% assessment guidelines, Table 11.2.

% crops is a structure with the crop info
Sc = crops.Sc; % species/type id
YI = crops.YI; % annual average crop yield
F = crops.F; % Fraction of AG residues left in field post-harvest

ipcc = SplitCSV('crop_ipcc_defaults.csv'); % download the default values
% IPCC gives specific values for slope (a), intercept (b),
% below-ground N content (crop_bgn), above-ground N content (crop_agn), 
% below-ground C content (crop_bgc), above-ground C content (crop_agc),
% crop root:shoot ratio (crop_rs)


% Find the species specific default parameters

for i=1:length(Sc)
    f = find(ipcc.Sc==Sc(i)); 
    R(i) = YI(i)*ipcc.a(f) + ipcc.b(f); % total above ground residues
    AG(i)= R(i)*F(i); % above-ground residues left in field
    BG(i) = (YI(i) + R(i))*ipcc.crop_rs(f); % below ground residues
    
    % Make residue inputs a vector the length of fire_occ
    Rag(:,i)= AG(i).*ones(length(fire_occ),1);
    Rbg(:,i)=BG(i).*ones(length(fire_occ),1);
    on_farm(:,i)=Rag(:,i); % residues dry matter available for fire to consumer on the field
end
        
% when fire occurs, reduce above-ground residues by 80%
f2=find(fire_occ==1);
Rag(f2,:)=Rag(f2,:).*0.2; %a ssumes a combustion factor of 0.8 for crop residues (IPCC)

for i=1:length(Sc)
    f = find(ipcc.Sc==Sc(i)); 
    crop_NI= Rag.*ipcc.crop_agn(f) + Rbg.*ipcc.crop_bgn(f).*crop30; % crop N inputs
    crop_CI= Rag.*ipcc.crop_agc(f) + Rbg.*ipcc.crop_bgc(f).*crop30; % crop C inputs
    off_farm(:,i)= (R(i)*(1-F(i))).*ones(length(fire_occ),1); % residue dry matter taken off farm
end

% And sum the soil inputs, and residues on and off farm for fire emissions
crop_NI=sum(crop_NI,2);
crop_CI=sum(crop_CI,2);
on_farm=sum(on_farm,2);
off_farm=sum(off_farm,2);

end
