function [a,b,xmod,ymod]=optimumhypcurve(xobs,yobs)

pars_i= [50 0.3];

pars_out=fminsearch(@(pars) costfunction(hypcurve(xobs, pars), yobs),  pars_i);

a=pars_out(1);
b=pars_out(2);
xmod=([0:1:100]);
ymod=hypcurve(xmod,pars_out);
end

function ymod=hypcurve(xobs,pars)
a=pars(1);
b=pars(2);
ymod=a*(1-exp(-b*xobs));
end

function rmse=costfunction(ymod,yobs)
rmse=sum((ymod-yobs).^2);
end
