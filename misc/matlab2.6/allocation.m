%% Figuring out the allocation model for miombo trees 
% based on data from Chidumayo (1997) p:89

% data from box 4.3
dbh = [2,3,4,5,6,7,9,11,13,14,15,20,25];
cord = [0.5,0.9,2.7,4.1,6.6,9.2,16.2,24.3,53.2,66.3,71.7,141.6,243]; % kg DM
brush = [2,2,3.3,4,4.8,6.7,8.5,9,20,23,18,24,38];
leaf = [0.4,0.4,0.6,0.7,0.9,1.3,1.7,1.2,4.2,4.1,2.8,4.1,8.4];
agb = cord+brush; % AGB is only stem and branches

b_a = brush./agb; % ratio of brush:agb

l_a = leaf./agb; % ratio of leaf:agb

c_a = cord./agb; % ratio of cord:agb

% create allocation model based on power law for branch and leaf allocation
% to dbh
% (see excel spreadsheet)
model_branch = @(x) 1.5266.*x.^-0.723; % where x is dbh in cm of a tree
model_stem = @(x) 1-(model_branch(x));
model_leaf = @(x) 0.297.*x.^-0.75;

% create dbh vector
DBH = (1:1:50);

% allocation based on dbh
al_branch = model_branch(DBH);
al_leaf = model_leaf(DBH);
al_stem = model_stem(DBH);

figure('Color', 'w')
plot(dbh,b_a,'b.',dbh,c_a,'g.',dbh,l_a,'r.')
hold on
plot(DBH,al_branch,'b-',DBH,al_stem,'g-', DBH,al_leaf,'r-')
xlabel('DBH (cm)'), ylabel('Allocation ratio')
legend('Branch data','Stem data', 'Leaf data','Branch modelled','Stem modelled','Leaf modelled')






    
