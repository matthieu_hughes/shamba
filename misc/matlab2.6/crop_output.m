function [crop_resid, crop_soilinp, crop_n_inp] = crop_output(crops, fire_occ, burn)

% This function calculates the crop residues available for fire to consume (t DM/ha),
% the crop C soil inputs (t C/ha) and crop N inputs (t N/ha) for every year
% of the model run.

% inputs are:
% crops -  a structure of crop data
% fire_occ - vector of fire occurance, 1 = fire, 0 = no fire
% burn - are crop residues burnt off the farm? 1 = yes, 0 = no.
    
[crop_agbinp, crop_bgbinp, crop_ninp, resid] =...
    crop(crops.hyield_wt,crops.hi,crops.resid,crops.crop_DMf,...
    crops.crop_c, crops.crop_n, crops.crop_rs, burn);

for i = 1:length(fire_occ) % calculate loss of crop soil inputs due to fire occurance
    if fire_occ(i)==1
        crop_fireloss(i,1)= crop_agbinp*0.8; % assumes a combustion factor of 0.8 for crop residues (IPCC)
        crop_n_inp(i,1) = crop_ninp*0.8;
    else crop_fireloss(i,1) = 0;
        crop_n_inp(i,1) = crop_ninp;
    end
end

crop_resid = ones(length(fire_occ),1).*resid; % total available residues for fire to burn (Mg DM/ha)
crop_soilinp = ones(length(fire_occ),1).*(crop_agbinp + crop_bgbinp) - crop_fireloss; % Mg C/ha

end
