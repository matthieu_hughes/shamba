function [tree_soilinp, tree_n_inp, total_biom, biom_growth, tree_fire] =...
    biomass2(TH, TO, THf, SD, AL, N, Mort, xobs, yobs, m, wood_rs, wood_c, root_30, fire_occ)

% this model outputs tree soil inputs (t C/ha), tree N inputs (t N/ha),
% total woody biomass (t C/ha) and amoutn of litter available for fire (t
% DM/ha) for every year of the model run.


% Set initial conditions
meanD(1) = 1; % must have some biomass to begin with
agb_inc(1) = 0;
D_inc(1)=Dinc(xobs,yobs,m,0); % initial increment modelled

startpool.agb(1) = 0; startpool.stem(1) = 0; % start of year pool
startpool.leaf(1) = 0; startpool.branch(1) = 0; startpool.froot(1) = 0; startpool.croot(1) = 0;

pool.agb(1) = 0; pool.stem(1) = 0; % total pool size
pool.leaf(1) = 0; pool.branch(1) = 0; pool.froot(1) = 0; pool.croot(1) = 0;

thin.stem(1) = 0;
thin.leaf(1) = 0; thin.branch(1) = 0; thin.froot(1) = 0; thin.lcroot(1) = 0;

soilinp.stem(1) = 0;
soilinp.leaf(1) = 0; soilinp.branch(1) = 0; soilinp.froot(1) = 0; soilinp.croot(1) = 0;

% Run the mass-balance model for each time step

for i = 2: length(fire_occ)
    
    SD(i,1) = (1-Mort(i,1)).*SD(i-1,1); % mortality first
    
    SD(i,1) = (1-TH(i,1)).*SD(i,1); % thinning then thinning
    
    D_inc(i,1) = Dinc(xobs,yobs,m,meanD(i-1)); % size dependant D_inc
    
    meanD(i,1) = meanD(i-1)+D_inc(i);
    
    agb_inc(i,1) = SD(i,1).*(ryan09(meanD(i))...
                 - ryan09(meanD(i-1))); % calculate AGB increment 
             
    % C pools growth based on allocations
    startpool.agb(i,1) = pool.agb(i-1) + agb_inc(i); % growth og AGB first
    startpool.stem(i,1) = startpool.agb(i) * AL.stem; 
    startpool.leaf(i,1) = startpool.agb(i) * AL.leaf;  
    startpool.branch(i,1) = startpool.agb(i) * AL.branch;
    startpool.froot(i,1) = startpool.agb(i) * AL.leaf; % assumes same allocation as leaves
    startpool.croot(i,1) = startpool.agb(i) * AL.croot; % same as root:shoot
    
    % Woody biomass annual increments (t C/ha/year)
    biom_growth(i,1) = (agb_inc(i,1) + (agb_inc(i,1) * wood_rs))./1000; % t C/ha growth of woody biomass
        
    % keep track of thinned mass (uses mass from the year before thinning).
    thin.stem(i,1) = pool.stem(i-1,1).*TH(i); 
    thin.leaf(i,1) = pool.leaf(i-1,1).*TH(i); % this doesn't work if pool is zero
    thin.branch(i,1) = pool.branch(i-1,1).*TH(i); 
    thin.froot(i,1) = pool.froot(i-1,1).*TH(i);  % this doesn't work if pool is zero
    thin.croot(i,1) = pool.croot(i-1,1).*TH(i); 
    
    % keep track of turned over mass
    turned.stem(i,1) = startpool.stem(i,1).* TO.stem;
    turned.leaf(i,1) = startpool.leaf(i,1).* TO.leaf; 
    turned.branch(i,1) = startpool.branch(i,1).* TO.branch;
    turned.froot(i,1) = startpool.froot(i,1).* TO.froot;
    turned.croot(i,1) = startpool.croot(i,1).* TO.croot;
    
    
    % keep track of the soil inputs (if fire, agb inputs are decreased by 74%)
    % based on IPCC combustion factor for savanna woodland (table 2.6)
    if fire_occ(i,1) == 1
        soilinp.stem(i,1) = (turned.stem(i,1) + thin.stem(i,1).*THf.stem).*(1-0.74);
        soilinp.leaf(i,1) = (turned.leaf(i,1) + thin.leaf(i,1).*THf.leaf).*(1-0.74);
        soilinp.branch(i,1) = (turned.branch(i,1) + thin.branch(i,1).*THf.branch).*(1-0.74);
      else   soilinp.stem(i,1) = turned.stem(i,1) + thin.stem(i,1).*THf.stem;
             soilinp.leaf(i,1) = turned.leaf(i,1) + thin.leaf(i,1).*THf.leaf; 
             soilinp.branch(i,1) = turned.branch(i,1) + thin.branch(i,1).*THf.branch;
    end
        
        soilinp.froot(i,1) = (turned.froot(i,1) + thin.froot(i,1).*THf.froot)*root_30 ;
        soilinp.croot(i,1) = (turned.croot(i,1) + thin.croot(i,1).*THf.croot)*root_30; % take account of roots in 0-30 cm
    
    % Get N inputs from each source (kg N/ha)
    n_inp.stem(i,1) = soilinp.stem(i)/wood_c*N.stem; % must convert soilinp from kgC to kg DM and then multiply by %N
    n_inp.branch(i,1) = soilinp.branch(i)/wood_c*N.branch;
    n_inp.leaf(i,1) = soilinp.leaf(i)/wood_c*N.leaf;
    n_inp.froot(i,1) = soilinp.froot(i)/wood_c*N.froot;
    n_inp.croot(i,1) = soilinp.croot(i)/wood_c*N.croot;
        
    % keep track of AGB litter available for fire to consume
    tree_fire(i,1) = ((turned.stem(i,1) + thin.stem(i,1).*THf.stem)...
        + (turned.branch(i,1) + thin.branch(i,1).*THf.branch)...
        + (turned.leaf(i,1) + thin.leaf(i,1).*THf.leaf))./1000./wood_c; % Mg DM/ha
        
    % keep track of off farm biomass (one pool)
    offfarm(i,1) = thin.stem(i,1).*(1-THf.stem)...
                  +thin.branch(i,1).*(1-THf.branch); % assumes only branches and stems are removed

     % biomass pools: AGB where AGB is stem and branches
    pool.agb(i,1) = startpool.agb (i,1)... % add appropriate fraction of agb_inc
        - (turned.stem(i,1) + turned.branch(i,1))... % turnover from start pool
        - (thin.stem(i,1) + thin.branch(i,1)); % remove mass to thinned pool
                  
    % stem
    pool.stem(i,1) = startpool.stem (i,1)... % add appropriate fraction of agb_inc
        - turned.stem(i,1)... % turnover from start pool
        - thin.stem(i,1); % remove mass to thinned pool
    
    %leaf
    pool.leaf(i,1) = startpool.leaf(i,1)...% add appropriate fraction of agb_inc
        - turned.leaf(i,1)... % turnover from start pool
        - thin.leaf(i,1); % remove mass to thinned pool
        
    %branch
    pool.branch(i,1) = startpool.branch(i,1)...% add appropriate fraction of agb_inc
        - turned.branch(i,1)- thin.branch(i,1);
    
    %fine roots
    pool.froot(i,1) = startpool.froot(i,1)...% add appropriate fraction of agb_inc
        - turned.froot(i,1) - thin.froot(i,1);
    
    %coarse roots
    pool.croot(i,1) = startpool.croot(i,1)...% add appropriate fraction of agb_inc
        - turned.croot(i,1) - thin.croot(i,1);
    
    meanD(i,1) = ryan09inv(pool.agb(i,1)./SD(i,1));% recalculate meanD now pools have incremented
  
end

tree_soilinp = (soilinp.stem+soilinp.branch+soilinp.leaf+soilinp.froot+soilinp.croot)./1000; % Mg C /ha
tree_n_inp = (n_inp.stem+n_inp.branch+n_inp.leaf+n_inp.froot+n_inp.croot)./1000; % Mg N/ha

basal_area = ((pi().*(meanD./2).^2)./10000).*SD; % m2/ha
LAI = (((startpool.leaf./0.5)./10000).*1000)./79; % m2 leaf area/m2 ground area. Assumes LMA = 79 g/m2

total_biom = (pool.agb+pool.croot)./1000; % total woody biomass tC/ha

% Plot it
figure('Color',[1 1 1])
subplot(3,2,1)
plot(meanD), title('meanD'), ylabel('DBH (cm)')
subplot(3,2,2)
plot(D_inc), title('D_i_n_c'), ylabel('DBH increment (cm)')
subplot(3,2,3)
plot([pool.stem pool.leaf pool.branch pool.froot pool.croot]./1000), title('pools'), ylabel('Mg C/ha')
legend('stem', 'leaf', 'branch','fine root', 'coarse root', 'Location', 'Best')
subplot(3,2,4)
plot([pool.agb pool.froot+pool.croot]./1000), title('AGB and BGG'),ylabel('Mg C/ha')
legend('AGB', 'BGB', 'Location', 'NorthWest')
subplot(3,2,5)
plot(SD), title('Stand density'), ylabel('No. trees/ha')


%figure('Color',[1 1 1])
%plot(meanD, al.stem, meanD, al.branch, meanD, al.leaf, [1 25], [wood_rs wood_rs])
%xlabel('DBH (cm)'), ylabel('Allocation ratio (mass C/ mass AGB C)')
%legend('Stem', 'Branch','Leaf and F.roots', 'C.roots', 'Location', 'Best')

figure('Color', [1 1 1])
subplot(2,2,1)
plot([soilinp.stem soilinp.branch soilinp.croot]./1000)
title('soil inputs'), ylabel('Mg C/ha')
legend('stem','branch','coarse root', 'Location', 'NorthWest')
subplot(2,2,2)
plot([soilinp.leaf soilinp.froot]./1000) 
title('soil inputs'), ylabel('Mg C/ha')
legend('leaf','fine roots', 'Location', 'NorthWest')
subplot(2,2,3)
plot(basal_area)
title('Basal area'), ylabel('m^2/ha')
subplot(2,2,4)
plot(LAI) 
title('Leaf area index'), ylabel('LAI (m^2 m^-^2')

end