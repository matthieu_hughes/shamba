%% Tillage calculation

map = sum(climate(:,1)); % mean annual precipitation

% The tillage change coefficient, k, is as follows
% Based on Ogle et al (2005) 
if map >= 1000
    if till == 2
        k = 1.23;
    else k = 1.16;
    end
else
    if till == 2
        k = 1.17;
    else k = 1.10;
    end
end

soc_till = soc(d)*k;
TILL = ones(d,1).*((soc_till-soc(d))*(44/12))/d; % t CO2e/ha/year



     
