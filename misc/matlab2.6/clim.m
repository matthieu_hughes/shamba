function [rate, DEFIC] = clim(climate,sc,clay)

% calculates the rate modyfying factor for RothC (see RothC manual) for moisture deficit,
% temperature and soil cover.

% NB. These equation assume that the first month has no water deficit (i.e.
% et does not exceed rainfall)!!!!


rain = climate(:,1);
temp = climate(:,2);
et = climate(:,3); % this is evapotranspiration, and must be * 0.75 to get pan evaporation 
e = et./0.75; % this is pan evaporation

% moisture deficit rate modifyer (b) /month

DEFIC = zeros(12,1); % clear each month's soil moisture deficit
b = ones(12,1);

% Find first month where rainfall exceeds evaporation (starting from January)
m=0;
m = find(rain>e,1,'first');
if isempty(m)  % when none of the months, including Jan, have evap > rain
    error('Warning: evaporation always > rainfall')
    m=1; % assumes Jan is the first month where rain>et
end

% find first month where rainfall < evaporation after the month found above
% where rainfall > evaporation

ibreak2=0;

for i= 1:12
    m=m+1;
    if m>12; m=1; end
    if rain(m)<e(m)
        ibreak2=1;
        break
    end
end
    


if ibreak2==0
    b = ones(12,1);
else
    m=m-1;
    if m<1; m=12; end
    
    x2=-(20+1.3.*clay-0.01.*(clay).^2);
    x2=x2*30/23; % max SMD adjusted for 30 cm soil depth
    y2=0.2;
    D1=0.556*x2; % max SMD when soil cover = 0
    D2=x2; % max SMD when soil cover =1
    x1=0.444.*x2;
    y1=1;
    
    DF=0; % Accumulated soil moisture deficit
    
    for i=1:12
        if rain(m)>et(m)
            DF=DF+(rain(m)-et(m));
            if DF>0; DF= 0; end
        else
            if sc(m)==1
                DF=DF+(rain(m)-et(m));
                if DF<D2; DF=D2; end
            else
                if DF<D1
                    %when no cro pis present and SMD < D1, make no change
                    %to SMD
                else
                    DF=DF+(rain(m)-et(m));
                    if DF<D1; DF = D1; end
                end
            end
        end
                      
        if DF>=x1
            b(m)=1;
        elseif DF>=x2
            b(m)= y2+(y1-y2)*((x2-DF)/(x2-x1));
        else
            'error occurred';
        end
    DEFIC(m)=DF;
    m=m+1;
    if m>12; m=1;end
    end
    
end % this ends the ibreak2==1
    
             
          
 % temperature rate modifier (a) /month

 a=zeros(12,1);
 for i = 1:12
     if temp(i)<-5
         a(i,1) = 0;
     else
         a(i,1) = 47.91./(exp(106.06./(temp(i)+18.27))+1);
     end
 end

% soil cover rate modifier (c) /month
c=zeros(12,1);
for i= 1:12
    if sc(i)==1
        c(i,1)=0.6;
    else c(i,1)=1;
    end
end


% Final rate modifyer from a b and c as a mean for the year

rate = mean(a.*b.*c);


end


        
        
        
        
