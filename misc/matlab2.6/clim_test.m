clear all

%temp = [20 20 20 15 20 20 20 20 5 20 20 20]';

%rain = [74 59 62 51 52 57 34 55 58 56 75 71]';

%et = ([8 10 27 49 83 99 103 91 83 99 103 91].*0.75)';

clim = SplitCSV('climate_equilibrium_test.csv');
temp = clim.Temp;
rain = clim.Rain;
et = clim.ET;

sc = [1 1 1 1 1 1 1 1 1 1 1 1]';

clay =13;

max_tsmd1 = (-(20+1.3*clay-0.01*clay^2))/23*30; % max top soil moisture deficit if soil is covered

max_tsmd2 = (-(20+1.3*clay-0.01*clay^2))/23*30/1.8; % max top soil moisture deficit if soil is not covered

def = rain-et;

ac_tsmd = zeros(1,length(rain));

for i=1:12
    if sc(i)==1
        max_tsmd(i,1)=max_tsmd1;
    else max_tsmd(i,1)=max_tsmd2;
    end
end

for i=2:length(rain)
    if def(i)>0 && ac_tsmd(i-1)+def(i)>=0
        ac_tsmd(i)=0;
    else if def(i)>0 && ac_tsmd(i-1)+def(i)<0
            ac_tsmd(i)= ac_tsmd(i-1) + def(i);
        else if def(i)<0 && ac_tsmd(i-1)+def(i) > max_tsmd(i);
        ac_tsmd(i)= ac_tsmd(i-1) + def(i);
            else ac_tsmd(i)= max_tsmd(i);
            end
        end
    end
end

b=ones(1,length(rain));

for i=2:length(rain)
    if ac_tsmd(i) > 0.444*max_tsmd1
        b(i) = 1;
    else b (i) = 0.2+(1-0.2)*(max_tsmd1-ac_tsmd(i))/(max_tsmd1-0.444*max_tsmd1);
    end
end

% temperature rate modifier (a) /month

a = 47.9./(1+(exp(106./(temp+18.25))));
                
% soil cover rate modifier (c) /month

for i= 1:length(sc)
    if sc(i)==1
         c(i,1)=0.6;
        else c(i,1)=1;
    end
end

[a b' c]

% Final rate modifyer from a b and c as a mean for the year

% rate = mean(a.*b'.*c)